cc.Class({
    extends: cc.Component,

    properties: {
        beginBtn: cc.Sprite,
        historyList: cc.Label,
        startBtn: cc.Sprite,
        openKefu1: cc.Label,
        openKefu2: cc.Label,
        commentContent: cc.Sprite
    },
    onLoad () {
        this.beginBtn.node.on(cc.Node.EventType.TOUCH_END, this.toPayScene, this)
        this.historyList.node.on(cc.Node.EventType.TOUCH_END, this.toHistoryScene, this)
        this.startBtn.node.on(cc.Node.EventType.TOUCH_END, this.toBeginBtnPosition, this)
        this.openKefu1.node.on(cc.Node.EventType.TOUCH_END, this.toOpenKeFu, this)
        this.openKefu2.node.on(cc.Node.EventType.TOUCH_END, this.toOpenKeFu, this)
        this.commentContent.node.on(cc.Node.EventType.TOUCH_END, this.commentAnimation, this)
    },
    toPayScene () {
        cc.director.loadScene("Pay")
    },
    toHistoryScene(){
        cc.director.loadScene("History")
    },
    toBeginBtnPosition(){
        console.log('toBeginBtnPosition')
    },
    toOpenKeFu(){
        console.log('toOpenKeFu')
    },
    commentAnimation(){
        console.log('commentAnimation')
    },
    start () {
        if(cc.sys.os === cc.sys.OS_ANDROID||cc.sys.os==cc.sys.OS_IOS){
            cc.view.setDesignResolutionSize(1080, 1920, cc.ResolutionPolicy.FIXED_HEIGHT);
        }else{
            cc.view.setDesignResolutionSize(1080, 1920, cc.ResolutionPolicy.SHOW_ALL);
        }
    },

    // update (dt) {},
});
