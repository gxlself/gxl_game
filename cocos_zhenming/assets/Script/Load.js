cc.Class({
    extends: cc.Component,

    properties: {
        
    },
    onLoad () {
        let _this = this
        cc.log("开始加载资源")
        cc.myAssets = {}
        cc.loader.loadResDir('Index', function (err, assets) {
            if (err) {
                cc.error(err);
                return;
            }
            cc.myAssets['Index'] = assets;
            cc.director.loadScene("Index")
            cc.log("加载资源结束")
        });
    },
    start () {
        if(cc.sys.os==cc.sys.OS_IOS){
            cc.view.setDesignResolutionSize(1080, 1920, cc.ResolutionPolicy.FIXED_HEIGHT);
        }
        // else if(cc.sys.os === cc.sys.OS_ANDROID){
        //     cc.view.setDesignResolutionSize(1080, 1920, cc.ResolutionPolicy.SHOW_ALL);
        // }
        else{
            cc.view.setDesignResolutionSize(1080, 1920, cc.ResolutionPolicy.SHOW_ALL);
        }
    }
});
