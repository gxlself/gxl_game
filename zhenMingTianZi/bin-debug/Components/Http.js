var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
// baseUrl:http请求接口地址
var baseUrl = GameConfig.getBasicUrl();
// version:后台定义的游戏id
var app_code = GameConfig.getAppCode();
// app_code:后台定义的游戏版本号
var version = GameConfig.getVersion();
var Api = (function () {
    function Api() {
    }
    /**
     * post请求数据，默认需要token才能请求
     * @param  {} url post地址
     * @param  {} data post数据，默认放在body内
     */
    Api.post = function (url, data) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        if (!Api.token) {
                            reject('token为空，请重新登陆');
                        }
                        var request = new egret.HttpRequest();
                        request.responseType = egret.HttpResponseType.TEXT;
                        request.open(url, egret.HttpMethod.POST);
                        request.setRequestHeader("Content-Type", "application/json");
                        request.setRequestHeader("Authorization", Api.token);
                        request.send(data);
                        request.addEventListener(egret.Event.COMPLETE, function (evt) {
                            var res = evt.currentTarget;
                            res.response ? resolve(JSON.parse(res.response)) : resolve({});
                        }, _this);
                        request.addEventListener(egret.IOErrorEvent.IO_ERROR, function (evt) {
                            reject(evt);
                        }, _this);
                    })];
            });
        });
    };
    /**
     * get请求数据
     * @param  {} url 请求地址
     * @param  {} noToken? 是否需要传入token，若为true，则不需要传token亦可访问
     */
    Api.get = function (url, noToken) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        if (!Api.token && !noToken) {
                            reject('token为空，请重新登陆');
                        }
                        var request = new egret.HttpRequest();
                        request.responseType = egret.HttpResponseType.TEXT;
                        request.open(url, egret.HttpMethod.GET);
                        request.setRequestHeader("Authorization", Api.token);
                        request.send();
                        request.addEventListener(egret.Event.COMPLETE, function (evt) {
                            var res = evt.currentTarget;
                            resolve(JSON.parse(res.response));
                        }, _this);
                        request.addEventListener(egret.IOErrorEvent.IO_ERROR, function (evt) {
                            reject(evt);
                        }, _this);
                    })];
            });
        });
    };
    Api.getToken = function () {
        return this.token;
    };
    /**
     * uploadRecords 上传成绩，调用后返回true即上传成功，返回false即上传失败
     * @param  {} record_data
     */
    Api.uploadRecords = function (record_data) {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        result = true;
                        // MD5签名
                        record_data = MD5.createSignObject(record_data);
                        return [4 /*yield*/, Api.post(Api.uploadRecordsPath, record_data).catch(function (e) {
                                result = false;
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, result];
                }
            });
        });
    };
    /**
     * getBestRecord 根据传入的时间范围，调用后返回传入时间范围内最佳成绩，如有错误，则返回null
     * @param  {} timeRange
     */
    Api.getBestRecord = function (record_type) {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        result = null;
                        return [4 /*yield*/, Api.get(Api.bestRecordPath + '?record_type=' + record_type).then(function (res) {
                                result = res;
                            }).catch(function (e) {
                                result = null;
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, result];
                }
            });
        });
    };
    /**
     * getRankings 调用后返回世界排行榜数据，如有错误，则返回null
     * @param  {} timeRange
     */
    Api.getRankings = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                result = null;
                // wx.showLoading({
                //     title: '排行榜加载中...',
                //     mask: true,
                //     success: () => { },
                //     fail: () => { },
                //     complete: () => { }
                // });
                // await Api.get(Api.getRankingsPath + '?record_type=1&page=1&per_page=200').then((res) => {
                //     result = res;
                // }).catch((e) => {
                //     result = null;
                // });
                // wx.hideLoading();
                return [2 /*return*/, result];
            });
        });
    };
    /**
     * getRankings 调用后返回分享图片链接，如有错误，则返回默认分享图地址
     * @param  {} timeRange
     */
    Api.getShareUrl = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        result = '';
                        return [4 /*yield*/, Api.get(Api.getShareUrlPath).then(function (res) {
                                result = res.url;
                            }).catch(function (e) {
                                result = '';
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, result];
                }
            });
        });
    };
    /**
     * 查询socket用的地址，return{tunnelId,connectUrl}
     */
    Api.getTunnel = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        result = null;
                        return [4 /*yield*/, Api.get(Api.tunnelPath).then(function (res) {
                                result = res;
                            }).catch(function (e) {
                                result = null;
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, result];
                }
            });
        });
    };
    Api.postEvent = function (event_type) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (!this.doPostEvent) {
                    return [2 /*return*/, true];
                }
                if (isNaN(event_type)) {
                    event_type = this.event_type.indexOf(event_type) + 1;
                }
                else if (!this.event_type[event_type]) {
                    event_type = 0;
                }
                if (event_type == 0) {
                    throw new Error('事件传值不是预设的值');
                }
                Api.post(Api.postEventPath, { event_type: event_type });
                return [2 /*return*/];
            });
        });
    };
    /**
     * 查询版本控制情况
     */
    Api.getConfiguration = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        result = null;
                        return [4 /*yield*/, Api.get(Api.configurationsPath + '?version=' + version + '&app_code=' + app_code, true)
                                .then(function (res) { result = res[0]; })
                                .catch(function (err) { console.warn(err); })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, result];
                }
            });
        });
    };
    /**
     * 登陆完成，获取token后调用以设置token
     * @param  {string} token
     */
    Api.setToken = function (token) {
        Api.token = token;
    };
    Api.baseUrl = baseUrl;
    // api地址
    Api.loginPath = baseUrl + '/game-plane/api/v1/auth_login'; //登录游戏
    Api.uploadRecordsPath = baseUrl + "/game-plane/api/v1/upload_record"; //上传成绩
    Api.bestRecordPath = baseUrl + "/game-plane/api/v2/best_record"; //获取最好成绩
    Api.getRankingsPath = baseUrl + "/game-plane/api/v2/week_rank_list"; //获取世界周排行榜
    Api.getShareUrlPath = baseUrl + "/game-plane/api/share_url"; //动态获取分享图片
    Api.postEventPath = baseUrl + "/game-plane/api/events"; //事件埋点上传
    Api.tunnelPath = baseUrl + "/game-plane/api/tunnel"; //获取socket地址
    Api.configurationsPath = baseUrl + "/game-plane/api/v1/config"; //获取游戏配置JSON
    // 事件类型字段                打开     单人游戏       上传成绩        重玩      分享           结果群排名     排行榜榜群排名  重生  
    Api.event_type = ['open', 'singleGame', 'uploadScore', 'replay', 'normalShare', 'groupResult', 'groupRank', 'reborn'];
    /**
     * 事件埋点
     * @param  {number} event_type
     */
    Api.doPostEvent = false;
    return Api;
}());
__reflect(Api.prototype, "Api");
//# sourceMappingURL=Http.js.map