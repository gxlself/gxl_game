var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var Main = (function (_super) {
    __extends(Main, _super);
    function Main() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Main.prototype.createChildren = function () {
        _super.prototype.createChildren.call(this);
        egret.lifecycle.addLifecycleListener(function (context) {
            // custom lifecycle plugin
        });
        egret.lifecycle.onPause = function () {
            egret.ticker.pause();
        };
        egret.lifecycle.onResume = function () {
            egret.ticker.resume();
        };
        //inject the custom material parser
        //注入自定义的素材解析器
        // let assetAdapter = new AssetAdapter();
        // egret.registerImplementation("eui.IAssetAdapter", assetAdapter);
        // egret.registerImplementation("eui.IThemeAdapter", new ThemeAdapter());
        this.runGame().catch(function (e) {
            console.log(e);
        });
    };
    Main.prototype.runGame = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadResource()];
                    case 1:
                        _a.sent();
                        this.createGameScene();
                        return [2 /*return*/];
                }
            });
        });
    };
    Main.prototype.loadResource = function () {
        return __awaiter(this, void 0, void 0, function () {
            var loadingView, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, , 5]);
                        loadingView = new LoadingUI();
                        this.stage.addChild(loadingView);
                        return [4 /*yield*/, RES.loadConfig("resource/default.res.json", "resource/")];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.loadTheme()];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, RES.loadGroup("preload", 0, loadingView)];
                    case 3:
                        _a.sent();
                        this.stage.removeChild(loadingView);
                        return [3 /*break*/, 5];
                    case 4:
                        e_1 = _a.sent();
                        console.error(e_1);
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    Main.prototype.loadTheme = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            // load skin theme configuration file, you can manually modify the file. And replace the default skin.
            //加载皮肤主题配置文件,可以手动修改这个文件。替换默认皮肤。
            var theme = new eui.Theme("resource/default.thm.json", _this.stage);
            theme.addEventListener(eui.UIEvent.COMPLETE, function () {
                resolve();
            }, _this);
        });
    };
    /**
     * 创建场景界面
     * Create scene interface
     */
    Main.prototype.createGameScene = function () {
        // 设置游戏参数内的默认舞台宽高
        GameConfig.setStageWidthHeight(this.stage);
        var stageW = this.stage.stageWidth;
        var stageH = this.stage.stageHeight;
        // 建立Scroller
        var indexScrollView = new egret.ScrollView();
        indexScrollView.bounces = false;
        indexScrollView.width = stageW;
        indexScrollView.height = stageH;
        indexScrollView.horizontalScrollPolicy = "off";
        indexScrollView.verticalScrollPolicy = "on";
        this.addChild(indexScrollView);
        var index_bgColor = new egret.Sprite();
        var index_bg = this.createBitmapByName("Bg_ZM_png");
        index_bg.x = 0;
        index_bg.y = 0;
        index_bg.width = indexScrollView.width;
        // 图片
        var zm_header = this.createBitmapByName("payBanner_ZM_png");
        var zm_personNum = this.createBitmapByName('testNum_ZM_png');
        var zm_intBox = new egret.Sprite();
        var zm_intBoxBg = this.createBitmapByName('zm-bg_png');
        var zm_usersThink = this.createBitmapByName("persent_png");
        var zm_xunAi = this.createBitmapByName("xunai_png");
        var zm_dashiContent = this.createBitmapByName("content_png");
        var zm_calculateBg = this.createBitmapByName("calculateBg_png");
        var zm_twoIm = this.createBitmapByName("twoIm_png");
        var zm_backPayBox = new egret.Sprite();
        var zm_backPayButton = this.createBitmapByName("btnBg_png");
        var zm_usersThinkTwo = this.createBitmapByName("persent_png");
        var zm_openKeFu = new egret.Sprite();
        var startTest = new egret.TextField();
        var zm_whenHappen = this.createBitmapByName("zm-when_png");
        var zm_title = this.createBitmapByName("zm-title_png");
        var zm_shaonian = this.createBitmapByName("zm-dashi_png");
        var zm_linePng = this.createBitmapByName("line_png");
        var zm_nameInput = new egret.TextField();
        // let copyBtn = 
        // 输入框设置
        zm_nameInput.type = egret.TextFieldType.INPUT;
        zm_nameInput.inputType = egret.TextFieldInputType.TEXT;
        zm_nameInput.width = stageW * 0.65;
        zm_nameInput.border = true;
        zm_nameInput.text = "请输入您的姓名";
        zm_nameInput.height = 70;
        zm_nameInput.backgroundColor = 0xf5e8c6;
        zm_nameInput.borderColor = 0xba0c0f;
        zm_nameInput.fontFamily = "Arial";
        zm_nameInput.textColor = 0x000000;
        zm_nameInput.size = 26;
        zm_nameInput.verticalAlign = "middle"; //设置垂直对齐方式
        // 需要单独设置的宽高
        zm_backPayButton.width = stageW;
        zm_intBox.width = stageW;
        zm_intBox.height = zm_intBoxBg.height;
        zm_backPayBox.width = stageW;
        zm_backPayBox.height = zm_backPayButton.height;
        // 输入栏目中元素位置/宽高
        zm_whenHappen.x = (zm_intBox.width - zm_whenHappen.width) / 2;
        zm_title.x = (zm_intBox.width - zm_title.width) / 2;
        zm_shaonian.x = (zm_intBox.width - zm_shaonian.width) / 2;
        zm_linePng.x = (zm_intBox.width - zm_linePng.width) / 2;
        zm_nameInput.x = (zm_intBox.width - zm_nameInput.width) / 2;
        zm_whenHappen.y = 80;
        zm_title.y = zm_whenHappen.y + zm_whenHappen.height + 20;
        zm_shaonian.y = zm_title.y + zm_title.height + 20;
        zm_linePng.y = zm_shaonian.y + zm_shaonian.height + 20;
        zm_nameInput.y = zm_linePng.y + zm_linePng.height + 10;
        // 按钮文本
        startTest.width = zm_backPayBox.width;
        startTest.height = zm_backPayBox.height;
        startTest.textAlign = "center"; //设置水平对齐方式
        startTest.verticalAlign = "middle"; //设置垂直对齐方式
        startTest.text = "开始测算";
        startTest.size = 30;
        startTest.textColor = 0xffffff;
        // 各自回家
        zm_intBox.addChild(zm_intBoxBg);
        zm_intBox.addChild(zm_whenHappen);
        zm_intBox.addChild(zm_title);
        zm_intBox.addChild(zm_shaonian);
        zm_intBox.addChild(zm_linePng);
        zm_intBox.addChild(zm_nameInput);
        zm_backPayBox.addChild(zm_backPayButton);
        zm_backPayBox.addChild(startTest);
        // 客服文本以及图像
        zm_openKeFu.width = stageW;
        var kefu_textOne = new egret.TextField();
        var kefu_textTwo = new egret.TextField();
        kefu_textOne.width = kefu_textTwo.width = stageW;
        kefu_textOne.height = kefu_textTwo.height = 40;
        kefu_textOne.textColor = kefu_textTwo.textColor = 0x000000; //设置颜色
        kefu_textOne.size = kefu_textTwo.size = 28; //设置文本字号
        kefu_textOne.textAlign = kefu_textTwo.textAlign = "center"; //设置水平对齐方式
        kefu_textOne.verticalAlign = kefu_textTwo.verticalAlign = "middle"; //设置垂直对齐方式
        kefu_textOne.y = 20;
        kefu_textTwo.y = kefu_textOne.y + kefu_textOne.height;
        var kefu_image = this.createBitmapByName("kefu_png");
        kefu_image.x = stageW / 2 - 170;
        kefu_image.y = kefu_textTwo.y - kefu_textTwo.height / 4.5;
        kefu_textOne.text = "如果需要帮助，请点此联系客服";
        kefu_textTwo.text = "我们的售后客服";
        var kefu_line = new egret.Shape();
        kefu_line.graphics.lineStyle(2, 0x000000);
        kefu_line.graphics.moveTo((kefu_image.x + kefu_image.width + 20), (kefu_textTwo.y + 33));
        kefu_line.graphics.lineTo(420, (kefu_textTwo.y + 33));
        kefu_line.graphics.endFill();
        // 添加到客服专栏
        zm_openKeFu.addChild(kefu_textOne);
        zm_openKeFu.addChild(kefu_textTwo);
        zm_openKeFu.addChild(kefu_image);
        zm_openKeFu.addChild(kefu_line);
        zm_openKeFu.height = kefu_textTwo.height * 2 + 60;
        // 分布高度
        zm_header.y = 0;
        zm_personNum.y = zm_header.height;
        zm_intBox.y = zm_personNum.y + zm_personNum.height;
        zm_usersThink.y = zm_intBox.y + zm_intBox.height;
        zm_xunAi.y = zm_usersThink.y + zm_usersThink.height;
        zm_dashiContent.y = zm_xunAi.y + zm_xunAi.height;
        zm_calculateBg.y = zm_dashiContent.y + zm_dashiContent.height;
        zm_twoIm.y = zm_calculateBg.y + zm_calculateBg.height;
        zm_backPayBox.y = zm_twoIm.y + zm_twoIm.height + 30;
        zm_usersThinkTwo.y = zm_backPayBox.y + zm_backPayBox.height + 30;
        zm_openKeFu.y = zm_usersThinkTwo.y + zm_usersThinkTwo.height;
        // 确定index_bg和高度值
        index_bg.height = zm_openKeFu.y + zm_openKeFu.height;
        index_bgColor.graphics.beginFill(0xEDC6B0);
        index_bgColor.graphics.drawRect(0, 0, stageW, index_bg.height);
        index_bgColor.graphics.endFill();
        // 需要居中的元素
        zm_header.x = (stageW - zm_header.width) / 2;
        zm_personNum.x = (stageW - zm_personNum.width) / 2;
        zm_intBox.x = (stageW - zm_intBox.width) / 2;
        zm_usersThink.x = (stageW - zm_usersThink.width) / 2;
        zm_xunAi.x = (stageW - zm_xunAi.width) / 2;
        zm_dashiContent.x = (stageW - zm_dashiContent.width) / 2;
        zm_calculateBg.x = (stageW - zm_calculateBg.width) / 2;
        zm_twoIm.x = (stageW - zm_twoIm.width) / 2;
        zm_usersThinkTwo.x = (stageW - zm_usersThinkTwo.width) / 2;
        zm_openKeFu.x = (stageW - zm_openKeFu.width) / 2;
        // 添加入scrollView滚动视图
        indexScrollView.setContent(index_bgColor);
        // 视图内部元素
        index_bgColor.addChild(index_bg);
        index_bgColor.addChild(zm_header);
        index_bgColor.addChild(zm_personNum);
        index_bgColor.addChild(zm_intBox);
        index_bgColor.addChild(zm_usersThink);
        index_bgColor.addChild(zm_xunAi);
        index_bgColor.addChild(zm_dashiContent);
        index_bgColor.addChild(zm_calculateBg);
        index_bgColor.addChild(zm_twoIm);
        index_bgColor.addChild(zm_backPayBox);
        index_bgColor.addChild(zm_usersThinkTwo);
        index_bgColor.addChild(zm_openKeFu);
        // 事件监听
        zm_openKeFu.addEventListener(egret.TouchEvent.TOUCH_TAP, openWxKefu, this);
        startTest.addEventListener(egret.TouchEvent.TOUCH_MOVE, backPayBtnPosition, this);
        function openWxKefu() {
            // 打开微信客服
            console.log("打开客服");
        }
        function backPayBtnPosition() {
            // 返回到开始测算按钮位置
            console.log("返回到开始测算按钮位置");
            var payBtnPosition = zm_backPayBox.y;
            indexScrollView.setScrollPosition(payBtnPosition, 0);
        }
    };
    /**
     * 根据name关键字创建一个Bitmap对象。name属性请参考resources/resource.json配置文件的内容。
     * Create a Bitmap object according to name keyword.As for the property of name please refer to the configuration file of resources/resource.json.
     */
    Main.prototype.createBitmapByName = function (name) {
        var result = new egret.Bitmap();
        var texture = RES.getRes(name);
        result.texture = texture;
        return result;
    };
    /**
     * 描述文件加载成功，开始播放动画
     * Description file loading is successful, start to play the animation
     */
    Main.prototype.startAnimation = function (result) {
        var _this = this;
        var parser = new egret.HtmlTextParser();
        var textflowArr = result.map(function (text) { return parser.parse(text); });
        var textfield = this.textfield;
        var count = -1;
        var change = function () {
            count++;
            if (count >= textflowArr.length) {
                count = 0;
            }
            var textFlow = textflowArr[count];
            // 切换描述内容
            // Switch to described content
            // textfield.textFlow = textFlow;
            var tw = egret.Tween.get(textfield);
            tw.to({ "alpha": 1 }, 200);
            tw.wait(2000);
            tw.to({ "alpha": 0 }, 200);
            tw.call(change, _this);
        };
        change();
    };
    /**
     * 点击按钮
     * Click the button
     */
    Main.prototype.onButtonClick = function (e) {
        var panel = new eui.Panel();
        panel.title = "Title";
        panel.horizontalCenter = 0;
        panel.verticalCenter = 0;
        this.addChild(panel);
    };
    return Main;
}(eui.UILayer));
__reflect(Main.prototype, "Main");
//# sourceMappingURL=Main.js.map