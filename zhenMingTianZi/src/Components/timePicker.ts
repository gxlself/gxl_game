class TimePicker {
	public constructor() {
		this.start()
	}

	private start(){
		let yearScroll: eui.Scroller = new eui.Scroller();  // 年滚动
		let monthScroll: eui.Scroller = new eui.Scroller(); // 月滚动
		let dayScroll: eui.Scroller = new eui.Scroller();   // 日滚动
		let hourScroll: eui.Scroller = new eui.Scroller();  // 时滚动
		let screenWidth: number =  GameConfig.getWidth();
		// 四分天下
		yearScroll.width = monthScroll.width = dayScroll.width = hourScroll.width = screenWidth / 4;
		// 高
		yearScroll.height = monthScroll.height = dayScroll.height = hourScroll.height = 60; 
		// y轴
		yearScroll.y = monthScroll.y = dayScroll.y = hourScroll.y = 0;
		// x轴
		yearScroll.x  = screenWidth / 4 * 0; 
		monthScroll.x = screenWidth / 4 * 1;
		dayScroll.x   = screenWidth / 2 * 2;
		hourScroll.x  = screenWidth / 2 * 3;
		// 滑动元素
		let yearList: eui.List = new eui.List();
		let monthList: eui.List = new eui.List();
		let dayList: eui.List = new eui.List();
		let hourList: eui.List = new eui.List();
			// 对用的数组
			let year: number[] = []
			let month: number[] = []
			let day: number[] = []
			let hour: any[] = ['不确定时']

			for (let i = 1900; i <= new Date().getFullYear(); i++) {
				year.push(i)
			}
			for (let i = 1; i <= 12; i++) {
				month.push(i)
			}
			for (let i = 1; i <= 31; i++) {
				day.push(i)
			}
			for (let i = 1; i <= 31; i++) {
				hour.push(i)
			}
			// 创建ArrayCollection实例
			yearList.dataProvider = new eui.ArrayCollection(year)
			monthList.dataProvider = new eui.ArrayCollection(month)
			dayList.dataProvider = new eui.ArrayCollection(day)
			hourList.dataProvider = new eui.ArrayCollection(hour)
			// 滚动视口绑定
			yearScroll.viewport = yearList
			dayScroll.viewport = monthList
			monthScroll.viewport = dayList
			hourScroll.viewport = hourList

			yearScroll.addEventListener(eui.UIEvent.CHANGE_END, moveScroller, this)
			monthScroll.addEventListener(eui.UIEvent.CHANGE_END, moveScroller, this);
			dayScroll.addEventListener(eui.UIEvent.CHANGE_END, moveScroller, this);
			hourScroll.addEventListener(eui.UIEvent.CHANGE_END, moveScroller, this);

			yearScroll.bounces = false
			monthScroll.bounces = false
			dayScroll.bounces = false
			let scrollBg = new egret.Sprite()
			scrollBg.y = 400
			scrollBg.addChild(yearScroll)
			scrollBg.addChild(monthScroll)
			scrollBg.addChild(dayScroll)
			scrollBg.addChild(hourScroll)
			GameConfig.getMain().addChild(scrollBg);

			let birth: string = ''

			function moveScroller() {
            if ((yearScroll.viewport.scrollV + yearScroll.height) >= yearScroll.viewport.contentHeight || yearScroll.viewport.scrollV < 0) {
                console.log("滚动到底部了");
                birth += year[year.length - 1]
            } else {
                let num = Math.floor(yearScroll.viewport.scrollV / 50)
                if (yearScroll.viewport.scrollV % 50 > 30) {
                    num++
                }
                birth += year[num]
                yearScroll.viewport.scrollV = num * 50
            }
            if ((monthScroll.viewport.scrollV + monthScroll.height) >= monthScroll.viewport.contentHeight || monthScroll.viewport.scrollV < 0) {
                console.log("滚动到底部了");
                birth += month[month.length - 1]
            } else {
                let num = Math.floor(monthScroll.viewport.scrollV / 50)
                if (monthScroll.viewport.scrollV % 50 > 30) {
                    num++
                }
                birth += month[num]
                monthScroll.viewport.scrollV = num * 50
            }
            if ((dayScroll.viewport.scrollV + dayScroll.height) >= dayScroll.viewport.contentHeight || dayScroll.viewport.scrollV < 0) {
                console.log("滚动到底部了");
                birth += day[day.length - 1]
            } else {
                let num = Math.floor(dayScroll.viewport.scrollV / 50)
                if (dayScroll.viewport.scrollV % 50 > 30) {
                    num++
                }
                birth += day[num]
                dayScroll.viewport.scrollV = num * 50
            }
            console.log(birth)
            birth = ""
        }
	}
}