class Main extends eui.UILayer {


    protected createChildren(): void {
        super.createChildren();

        egret.lifecycle.addLifecycleListener((context) => {
            // custom lifecycle plugin
        })

        egret.lifecycle.onPause = () => {
            egret.ticker.pause();
        }

        egret.lifecycle.onResume = () => {
            egret.ticker.resume();
        }

        //inject the custom material parser
        //注入自定义的素材解析器
        // let assetAdapter = new AssetAdapter();
        // egret.registerImplementation("eui.IAssetAdapter", assetAdapter);
        // egret.registerImplementation("eui.IThemeAdapter", new ThemeAdapter());

        this.runGame().catch(e => {
            console.log(e);
        })
    }

    private async runGame() {
        await this.loadResource()
        this.createGameScene();

        // const result = await RES.getResAsync("description_json")
        // this.startAnimation(result);
        // await platform.login();
        // const userInfo = await platform.getUserInfo();
        // console.log(userInfo);
    }

    private async loadResource() {
        try {
            const loadingView = new LoadingUI();
            this.stage.addChild(loadingView);
            await RES.loadConfig("resource/default.res.json", "resource/");
            await this.loadTheme();
            await RES.loadGroup("preload", 0, loadingView);
            this.stage.removeChild(loadingView);
        }
        catch (e) {
            console.error(e);
        }
    }
    private loadTheme() {
        return new Promise((resolve, reject) => {
            // load skin theme configuration file, you can manually modify the file. And replace the default skin.
            //加载皮肤主题配置文件,可以手动修改这个文件。替换默认皮肤。
            let theme = new eui.Theme("resource/default.thm.json", this.stage);
            theme.addEventListener(eui.UIEvent.COMPLETE, () => {
                resolve();
            }, this);
            
        })
    }

    private textfield: egret.TextField;
    /**
     * 创建场景界面
     * Create scene interface
     */
    protected createGameScene(): void {
        // 设置游戏参数内的默认舞台宽高
        GameConfig.setStageWidthHeight(this.stage)
        let stageW = this.stage.stageWidth;
        let stageH = this.stage.stageHeight;

        // 建立Scroller
        let indexScrollView:egret.ScrollView = new egret.ScrollView()
        indexScrollView.bounces = false
        indexScrollView.width = stageW;
        indexScrollView.height = stageH;
        indexScrollView.horizontalScrollPolicy = "off";
        indexScrollView.verticalScrollPolicy = "on";
        this.addChild(indexScrollView)
        let index_bgColor:egret.Sprite = new egret.Sprite();
        
        let index_bg:egret.Bitmap = this.createBitmapByName("Bg_ZM_png")
        index_bg.x = 0
        index_bg.y = 0
        index_bg.width = indexScrollView.width
        // 图片
        let zm_header:egret.Bitmap = this.createBitmapByName("payBanner_ZM_png")
        let zm_personNum:egret.Bitmap = this.createBitmapByName('testNum_ZM_png')
        let zm_intBox: egret.Sprite = new egret.Sprite()
        let zm_intBoxBg:egret.Bitmap = this.createBitmapByName('zm-bg_png')
        let zm_usersThink:egret.Bitmap = this.createBitmapByName("persent_png")
        let zm_xunAi:egret.Bitmap = this.createBitmapByName("xunai_png")
        let zm_dashiContent:egret.Bitmap = this.createBitmapByName("content_png")
        let zm_calculateBg:egret.Bitmap = this.createBitmapByName("calculateBg_png")
        let zm_twoIm:egret.Bitmap = this.createBitmapByName("twoIm_png")
        let zm_backPayBox:egret.Sprite = new egret.Sprite()
        let zm_backPayButton:egret.Bitmap = this.createBitmapByName("btnBg_png")
        let zm_usersThinkTwo:egret.Bitmap = this.createBitmapByName("persent_png")
        let zm_openKeFu:egret.Sprite = new egret.Sprite()
        
        let startTest:egret.TextField = new egret.TextField()
        let zm_whenHappen:egret.Bitmap = this.createBitmapByName("zm-when_png")
        let zm_title:egret.Bitmap = this.createBitmapByName("zm-title_png")
        let zm_shaonian:egret.Bitmap = this.createBitmapByName("zm-dashi_png")
        let zm_linePng:egret.Bitmap = this.createBitmapByName("line_png")

        let zm_nameInput:egret.TextField = new egret.TextField()
        // let copyBtn = 
        // 输入框设置
        zm_nameInput.type = egret.TextFieldType.INPUT;
        zm_nameInput.inputType = egret.TextFieldInputType.TEXT;
        zm_nameInput.width = stageW * 0.65
        zm_nameInput.border = true
        zm_nameInput.text = "请输入您的姓名"
        zm_nameInput.height = 70
        zm_nameInput.backgroundColor = 0xf5e8c6
        zm_nameInput.borderColor = 0xba0c0f;
        zm_nameInput.fontFamily = "Arial";
        zm_nameInput.textColor = 0x000000;
        zm_nameInput.size = 26;
        zm_nameInput.verticalAlign = "middle";//设置垂直对齐方式
        // 需要单独设置的宽高
        zm_backPayButton.width = stageW
        zm_intBox.width = stageW
        zm_intBox.height = zm_intBoxBg.height
        zm_backPayBox.width = stageW
        zm_backPayBox.height = zm_backPayButton.height

        // 输入栏目中元素位置/宽高
        zm_whenHappen.x = (zm_intBox.width - zm_whenHappen.width) / 2
        zm_title.x = (zm_intBox.width - zm_title.width) / 2
        zm_shaonian.x = (zm_intBox.width - zm_shaonian.width) / 2
        zm_linePng.x = (zm_intBox.width - zm_linePng.width) / 2
        zm_nameInput.x = (zm_intBox.width - zm_nameInput.width) / 2
        zm_whenHappen.y = 80
        zm_title.y = zm_whenHappen.y + zm_whenHappen.height + 20
        zm_shaonian.y = zm_title.y + zm_title.height + 20
        zm_linePng.y = zm_shaonian.y + zm_shaonian.height + 20
        zm_nameInput.y =  zm_linePng.y +  zm_linePng.height + 10

        // 按钮文本
        startTest.width = zm_backPayBox.width
        startTest.height = zm_backPayBox.height
        startTest.textAlign = "center";//设置水平对齐方式
        startTest.verticalAlign = "middle";//设置垂直对齐方式
        startTest.text = "开始测算"
        startTest.size = 30
        startTest.textColor = 0xffffff

        // 各自回家
        zm_intBox.addChild(zm_intBoxBg)
        zm_intBox.addChild(zm_whenHappen)
        zm_intBox.addChild(zm_title)
        zm_intBox.addChild(zm_shaonian)
        zm_intBox.addChild(zm_linePng)
        zm_intBox.addChild(zm_nameInput)
        zm_backPayBox.addChild(zm_backPayButton) 
        zm_backPayBox.addChild(startTest) 

            // 客服文本以及图像
            zm_openKeFu.width = stageW
            let kefu_textOne:egret.TextField = new egret.TextField();
            let kefu_textTwo:egret.TextField = new egret.TextField();
            kefu_textOne.width = kefu_textTwo.width = stageW
            kefu_textOne.height = kefu_textTwo.height = 40
            kefu_textOne.textColor = kefu_textTwo.textColor = 0x000000;//设置颜色
            kefu_textOne.size = kefu_textTwo.size = 28;//设置文本字号
            kefu_textOne.textAlign = kefu_textTwo.textAlign = "center";//设置水平对齐方式
            kefu_textOne.verticalAlign = kefu_textTwo.verticalAlign = "middle";//设置垂直对齐方式

            kefu_textOne.y = 20
            kefu_textTwo.y = kefu_textOne.y + kefu_textOne.height;

            let kefu_image:egret.Bitmap = this.createBitmapByName("kefu_png")
            kefu_image.x = stageW / 2 - 170
            kefu_image.y = kefu_textTwo.y - kefu_textTwo.height / 4.5
            kefu_textOne.text = "如果需要帮助，请点此联系客服"
            kefu_textTwo.text = "我们的售后客服"

            let kefu_line: egret.Shape = new egret.Shape()
            kefu_line.graphics.lineStyle( 2, 0x000000 );
            kefu_line.graphics.moveTo( (kefu_image.x + kefu_image.width + 20), (kefu_textTwo.y + 33) );
            kefu_line.graphics.lineTo( 420, (kefu_textTwo.y + 33) );
            kefu_line.graphics.endFill();
            // 添加到客服专栏
            zm_openKeFu.addChild(kefu_textOne)
            zm_openKeFu.addChild(kefu_textTwo)
            zm_openKeFu.addChild(kefu_image)
            zm_openKeFu.addChild(kefu_line)

            zm_openKeFu.height = kefu_textTwo.height * 2 + 60

        // 分布高度
        zm_header.y = 0
        zm_personNum.y = zm_header.height
        zm_intBox.y = zm_personNum.y + zm_personNum.height
        zm_usersThink.y = zm_intBox.y +  zm_intBox.height
        zm_xunAi.y = zm_usersThink.y + zm_usersThink.height
        zm_dashiContent.y = zm_xunAi.y + zm_xunAi.height
        zm_calculateBg.y = zm_dashiContent.y + zm_dashiContent.height
        zm_twoIm.y = zm_calculateBg.y + zm_calculateBg.height
        zm_backPayBox.y = zm_twoIm.y + zm_twoIm.height + 30
        zm_usersThinkTwo.y = zm_backPayBox.y + zm_backPayBox.height + 30
        zm_openKeFu.y = zm_usersThinkTwo.y + zm_usersThinkTwo.height

        // 确定index_bg和高度值
        index_bg.height = zm_openKeFu.y + zm_openKeFu.height
        index_bgColor.graphics.beginFill( 0xEDC6B0 );
        index_bgColor.graphics.drawRect( 0, 0, stageW, index_bg.height);
        index_bgColor.graphics.endFill();

        // 需要居中的元素
        zm_header.x = (stageW - zm_header.width) / 2
        zm_personNum.x = (stageW - zm_personNum.width) / 2
        zm_intBox.x = (stageW - zm_intBox.width) / 2
        zm_usersThink.x = (stageW - zm_usersThink.width) / 2
        zm_xunAi.x = (stageW - zm_xunAi.width) / 2
        zm_dashiContent.x = (stageW - zm_dashiContent.width) / 2
        zm_calculateBg.x = (stageW - zm_calculateBg.width) / 2
        zm_twoIm.x = (stageW - zm_twoIm.width) / 2
        zm_usersThinkTwo.x = (stageW - zm_usersThinkTwo.width) / 2
        zm_openKeFu.x = (stageW - zm_openKeFu.width) / 2

        // 添加入scrollView滚动视图
        indexScrollView.setContent(index_bgColor)

        // 视图内部元素
        index_bgColor.addChild(index_bg);
        index_bgColor.addChild(zm_header);
        index_bgColor.addChild(zm_personNum);
        index_bgColor.addChild(zm_intBox);
        index_bgColor.addChild(zm_usersThink);
        index_bgColor.addChild(zm_xunAi);
        index_bgColor.addChild(zm_dashiContent);
        index_bgColor.addChild(zm_calculateBg);
        index_bgColor.addChild(zm_twoIm);
        index_bgColor.addChild(zm_backPayBox);
        index_bgColor.addChild(zm_usersThinkTwo);
        index_bgColor.addChild(zm_openKeFu);

        // 事件监听
        zm_openKeFu.addEventListener(egret.TouchEvent.TOUCH_TAP, openWxKefu, this)
        startTest.addEventListener(egret.TouchEvent.TOUCH_MOVE, backPayBtnPosition, this)
        function openWxKefu(){
            // 打开微信客服
            console.log("打开客服")
        }
        function backPayBtnPosition(){
            // 返回到开始测算按钮位置
            console.log("返回到开始测算按钮位置")
            let payBtnPosition:number = zm_backPayBox.y
            indexScrollView.setScrollPosition(payBtnPosition, 0)
        }
    }
    /**
     * 根据name关键字创建一个Bitmap对象。name属性请参考resources/resource.json配置文件的内容。
     * Create a Bitmap object according to name keyword.As for the property of name please refer to the configuration file of resources/resource.json.
     */
    private createBitmapByName(name: string): egret.Bitmap {
        let result = new egret.Bitmap();
        let texture: egret.Texture = RES.getRes(name);
        result.texture = texture;
        return result;
    }
    /**
     * 描述文件加载成功，开始播放动画
     * Description file loading is successful, start to play the animation
     */
    private startAnimation(result: Array<any>): void {
        let parser = new egret.HtmlTextParser();

        let textflowArr = result.map(text => parser.parse(text));
        let textfield = this.textfield;
        let count = -1;
        let change = () => {
            count++;
            if (count >= textflowArr.length) {
                count = 0;
            }
            let textFlow = textflowArr[count];

            // 切换描述内容
            // Switch to described content
            // textfield.textFlow = textFlow;
            let tw = egret.Tween.get(textfield);
            tw.to({ "alpha": 1 }, 200);
            tw.wait(2000);
            tw.to({ "alpha": 0 }, 200);
            tw.call(change, this);
        };

        change();
    }

    /**
     * 点击按钮
     * Click the button
     */
    private onButtonClick(e: egret.TouchEvent) {
        let panel = new eui.Panel();
        panel.title = "Title";
        panel.horizontalCenter = 0;
        panel.verticalCenter = 0;
        this.addChild(panel);
    }
}
