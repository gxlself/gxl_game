var egret = window.egret;var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
/**
 * 统一设置游戏所有配置参数，含版本号，参数地址等
 *
 */
var GameConfig = (function () {
    function GameConfig() {
    }
    GameConfig.getBasicUrl = function () { return this.basicUrl; };
    ;
    GameConfig.getAppCode = function () { return this.appCode; };
    ;
    GameConfig.getVersion = function () { return this.version; };
    ;
    GameConfig.getShareTitle = function () { return this.shareTitle; };
    ;
    GameConfig.getShareImg = function () { return this.shareImg; };
    ;
    GameConfig.setStageWidthHeight = function (stage) { this.stageWidth = stage.stageWidth; this.stageHeight = stage.stageHeight; };
    GameConfig.getWidth = function () { return this.stageWidth; };
    ;
    GameConfig.getHeight = function () { return this.stageHeight; };
    ;
    GameConfig.getKey = function () { return this.key; };
    ;
    GameConfig.setMain = function (main) {
        this.stage = main;
    };
    GameConfig.getMain = function () {
        return this.stage;
    };
    // http通讯地址,请自行填入自己的服务器地址，若有跨域问题则点开开发工具详情，勾选不校验合法域名
    GameConfig.basicUrl = "";
    // 游戏自定义ID
    GameConfig.appCode = 1;
    // 游戏版本号
    GameConfig.version = "1.0.0";
    // 游戏基本分享标题
    GameConfig.shareTitle = "分享标题";
    // 游戏基本分享图片
    GameConfig.shareImg = "imgUrl";
    // 游戏基本宽
    GameConfig.stageWidth = 0;
    // 游戏基本高
    GameConfig.stageHeight = 0;
    // 游戏KEY
    GameConfig.key = '';
    // 游戏stage
    GameConfig.stage = null;
    return GameConfig;
}());
__reflect(GameConfig.prototype, "GameConfig");
// baseUrl:http请求接口地址
var baseUrl = GameConfig.getBasicUrl();
// version:后台定义的游戏id
var app_code = GameConfig.getAppCode();
// app_code:后台定义的游戏版本号
var version = GameConfig.getVersion();
var Api = (function () {
    function Api() {
    }
    /**
     * post请求数据，默认需要token才能请求
     * @param  {} url post地址
     * @param  {} data post数据，默认放在body内
     */
    Api.post = function (url, data) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        if (!Api.token) {
                            reject('token为空，请重新登陆');
                        }
                        var request = new egret.HttpRequest();
                        request.responseType = egret.HttpResponseType.TEXT;
                        request.open(url, egret.HttpMethod.POST);
                        request.setRequestHeader("Content-Type", "application/json");
                        request.setRequestHeader("Authorization", Api.token);
                        request.send(data);
                        request.addEventListener(egret.Event.COMPLETE, function (evt) {
                            var res = evt.currentTarget;
                            res.response ? resolve(JSON.parse(res.response)) : resolve({});
                        }, _this);
                        request.addEventListener(egret.IOErrorEvent.IO_ERROR, function (evt) {
                            reject(evt);
                        }, _this);
                    })];
            });
        });
    };
    /**
     * get请求数据
     * @param  {} url 请求地址
     * @param  {} noToken? 是否需要传入token，若为true，则不需要传token亦可访问
     */
    Api.get = function (url, noToken) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        if (!Api.token && !noToken) {
                            reject('token为空，请重新登陆');
                        }
                        var request = new egret.HttpRequest();
                        request.responseType = egret.HttpResponseType.TEXT;
                        request.open(url, egret.HttpMethod.GET);
                        request.setRequestHeader("Authorization", Api.token);
                        request.send();
                        request.addEventListener(egret.Event.COMPLETE, function (evt) {
                            var res = evt.currentTarget;
                            resolve(JSON.parse(res.response));
                        }, _this);
                        request.addEventListener(egret.IOErrorEvent.IO_ERROR, function (evt) {
                            reject(evt);
                        }, _this);
                    })];
            });
        });
    };
    Api.getToken = function () {
        return this.token;
    };
    /**
     * uploadRecords 上传成绩，调用后返回true即上传成功，返回false即上传失败
     * @param  {} record_data
     */
    Api.uploadRecords = function (record_data) {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        result = true;
                        // MD5签名
                        record_data = MD5.createSignObject(record_data);
                        return [4 /*yield*/, Api.post(Api.uploadRecordsPath, record_data).catch(function (e) {
                                result = false;
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, result];
                }
            });
        });
    };
    /**
     * getBestRecord 根据传入的时间范围，调用后返回传入时间范围内最佳成绩，如有错误，则返回null
     * @param  {} timeRange
     */
    Api.getBestRecord = function (record_type) {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        result = null;
                        return [4 /*yield*/, Api.get(Api.bestRecordPath + '?record_type=' + record_type).then(function (res) {
                                result = res;
                            }).catch(function (e) {
                                result = null;
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, result];
                }
            });
        });
    };
    /**
     * getRankings 调用后返回世界排行榜数据，如有错误，则返回null
     * @param  {} timeRange
     */
    Api.getRankings = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                result = null;
                // wx.showLoading({
                //     title: '排行榜加载中...',
                //     mask: true,
                //     success: () => { },
                //     fail: () => { },
                //     complete: () => { }
                // });
                // await Api.get(Api.getRankingsPath + '?record_type=1&page=1&per_page=200').then((res) => {
                //     result = res;
                // }).catch((e) => {
                //     result = null;
                // });
                // wx.hideLoading();
                return [2 /*return*/, result];
            });
        });
    };
    /**
     * getRankings 调用后返回分享图片链接，如有错误，则返回默认分享图地址
     * @param  {} timeRange
     */
    Api.getShareUrl = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        result = '';
                        return [4 /*yield*/, Api.get(Api.getShareUrlPath).then(function (res) {
                                result = res.url;
                            }).catch(function (e) {
                                result = '';
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, result];
                }
            });
        });
    };
    /**
     * 查询socket用的地址，return{tunnelId,connectUrl}
     */
    Api.getTunnel = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        result = null;
                        return [4 /*yield*/, Api.get(Api.tunnelPath).then(function (res) {
                                result = res;
                            }).catch(function (e) {
                                result = null;
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, result];
                }
            });
        });
    };
    Api.postEvent = function (event_type) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (!this.doPostEvent) {
                    return [2 /*return*/, true];
                }
                if (isNaN(event_type)) {
                    event_type = this.event_type.indexOf(event_type) + 1;
                }
                else if (!this.event_type[event_type]) {
                    event_type = 0;
                }
                if (event_type == 0) {
                    throw new Error('事件传值不是预设的值');
                }
                Api.post(Api.postEventPath, { event_type: event_type });
                return [2 /*return*/];
            });
        });
    };
    /**
     * 查询版本控制情况
     */
    Api.getConfiguration = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        result = null;
                        return [4 /*yield*/, Api.get(Api.configurationsPath + '?version=' + version + '&app_code=' + app_code, true)
                                .then(function (res) { result = res[0]; })
                                .catch(function (err) { console.warn(err); })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, result];
                }
            });
        });
    };
    /**
     * 登陆完成，获取token后调用以设置token
     * @param  {string} token
     */
    Api.setToken = function (token) {
        Api.token = token;
    };
    Api.baseUrl = baseUrl;
    // api地址
    Api.loginPath = baseUrl + '/game-plane/api/v1/auth_login'; //登录游戏
    Api.uploadRecordsPath = baseUrl + "/game-plane/api/v1/upload_record"; //上传成绩
    Api.bestRecordPath = baseUrl + "/game-plane/api/v2/best_record"; //获取最好成绩
    Api.getRankingsPath = baseUrl + "/game-plane/api/v2/week_rank_list"; //获取世界周排行榜
    Api.getShareUrlPath = baseUrl + "/game-plane/api/share_url"; //动态获取分享图片
    Api.postEventPath = baseUrl + "/game-plane/api/events"; //事件埋点上传
    Api.tunnelPath = baseUrl + "/game-plane/api/tunnel"; //获取socket地址
    Api.configurationsPath = baseUrl + "/game-plane/api/v1/config"; //获取游戏配置JSON
    // 事件类型字段                打开     单人游戏       上传成绩        重玩      分享           结果群排名     排行榜榜群排名  重生  
    Api.event_type = ['open', 'singleGame', 'uploadScore', 'replay', 'normalShare', 'groupResult', 'groupRank', 'reborn'];
    /**
     * 事件埋点
     * @param  {number} event_type
     */
    Api.doPostEvent = false;
    return Api;
}());
__reflect(Api.prototype, "Api");
var Main = (function (_super) {
    __extends(Main, _super);
    function Main() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Main.prototype.createChildren = function () {
        _super.prototype.createChildren.call(this);
        egret.lifecycle.addLifecycleListener(function (context) {
            // custom lifecycle plugin
        });
        egret.lifecycle.onPause = function () {
            egret.ticker.pause();
        };
        egret.lifecycle.onResume = function () {
            egret.ticker.resume();
        };
        //inject the custom material parser
        //注入自定义的素材解析器
        // let assetAdapter = new AssetAdapter();
        // egret.registerImplementation("eui.IAssetAdapter", assetAdapter);
        // egret.registerImplementation("eui.IThemeAdapter", new ThemeAdapter());
        this.runGame().catch(function (e) {
            console.log(e);
        });
    };
    Main.prototype.runGame = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadResource()];
                    case 1:
                        _a.sent();
                        this.createGameScene();
                        return [2 /*return*/];
                }
            });
        });
    };
    Main.prototype.loadResource = function () {
        return __awaiter(this, void 0, void 0, function () {
            var loadingView, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, , 5]);
                        loadingView = new LoadingUI();
                        this.stage.addChild(loadingView);
                        return [4 /*yield*/, RES.loadConfig("resource/default.res.json", "resource/")];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.loadTheme()];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, RES.loadGroup("preload", 0, loadingView)];
                    case 3:
                        _a.sent();
                        this.stage.removeChild(loadingView);
                        return [3 /*break*/, 5];
                    case 4:
                        e_1 = _a.sent();
                        console.error(e_1);
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    Main.prototype.loadTheme = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            // load skin theme configuration file, you can manually modify the file. And replace the default skin.
            //加载皮肤主题配置文件,可以手动修改这个文件。替换默认皮肤。
            var theme = new eui.Theme("resource/default.thm.json", _this.stage);
            theme.addEventListener(eui.UIEvent.COMPLETE, function () {
                resolve();
            }, _this);
        });
    };
    /**
     * 创建场景界面
     * Create scene interface
     */
    Main.prototype.createGameScene = function () {
        // 设置游戏参数内的默认舞台宽高
        GameConfig.setStageWidthHeight(this.stage);
        var stageW = this.stage.stageWidth;
        var stageH = this.stage.stageHeight;
        // 建立Scroller
        var indexScrollView = new egret.ScrollView();
        indexScrollView.bounces = false;
        indexScrollView.width = stageW;
        indexScrollView.height = stageH;
        indexScrollView.horizontalScrollPolicy = "off";
        indexScrollView.verticalScrollPolicy = "on";
        this.addChild(indexScrollView);
        var index_bgColor = new egret.Sprite();
        var index_bg = this.createBitmapByName("Bg_ZM_png");
        index_bg.x = 0;
        index_bg.y = 0;
        index_bg.width = indexScrollView.width;
        // 图片
        var zm_header = this.createBitmapByName("payBanner_ZM_png");
        var zm_personNum = this.createBitmapByName('testNum_ZM_png');
        var zm_intBox = new egret.Sprite();
        var zm_intBoxBg = this.createBitmapByName('zm-bg_png');
        var zm_usersThink = this.createBitmapByName("persent_png");
        var zm_xunAi = this.createBitmapByName("xunai_png");
        var zm_dashiContent = this.createBitmapByName("content_png");
        var zm_calculateBg = this.createBitmapByName("calculateBg_png");
        var zm_twoIm = this.createBitmapByName("twoIm_png");
        var zm_backPayBox = new egret.Sprite();
        var zm_backPayButton = this.createBitmapByName("btnBg_png");
        var zm_usersThinkTwo = this.createBitmapByName("persent_png");
        var zm_openKeFu = new egret.Sprite();
        var startTest = new egret.TextField();
        var zm_whenHappen = this.createBitmapByName("zm-when_png");
        var zm_title = this.createBitmapByName("zm-title_png");
        var zm_shaonian = this.createBitmapByName("zm-dashi_png");
        var zm_linePng = this.createBitmapByName("line_png");
        var zm_nameInput = new egret.TextField();
        // let copyBtn = 
        // 输入框设置
        zm_nameInput.type = egret.TextFieldType.INPUT;
        zm_nameInput.inputType = egret.TextFieldInputType.TEXT;
        zm_nameInput.width = stageW * 0.65;
        zm_nameInput.border = true;
        zm_nameInput.text = "请输入您的姓名";
        zm_nameInput.height = 70;
        zm_nameInput.backgroundColor = 0xf5e8c6;
        zm_nameInput.borderColor = 0xba0c0f;
        zm_nameInput.fontFamily = "Arial";
        zm_nameInput.textColor = 0x000000;
        zm_nameInput.size = 26;
        zm_nameInput.verticalAlign = "middle"; //设置垂直对齐方式
        // 需要单独设置的宽高
        zm_backPayButton.width = stageW;
        zm_intBox.width = stageW;
        zm_intBox.height = zm_intBoxBg.height;
        zm_backPayBox.width = stageW;
        zm_backPayBox.height = zm_backPayButton.height;
        // 输入栏目中元素位置/宽高
        zm_whenHappen.x = (zm_intBox.width - zm_whenHappen.width) / 2;
        zm_title.x = (zm_intBox.width - zm_title.width) / 2;
        zm_shaonian.x = (zm_intBox.width - zm_shaonian.width) / 2;
        zm_linePng.x = (zm_intBox.width - zm_linePng.width) / 2;
        zm_nameInput.x = (zm_intBox.width - zm_nameInput.width) / 2;
        zm_whenHappen.y = 80;
        zm_title.y = zm_whenHappen.y + zm_whenHappen.height + 20;
        zm_shaonian.y = zm_title.y + zm_title.height + 20;
        zm_linePng.y = zm_shaonian.y + zm_shaonian.height + 20;
        zm_nameInput.y = zm_linePng.y + zm_linePng.height + 10;
        // 按钮文本
        startTest.width = zm_backPayBox.width;
        startTest.height = zm_backPayBox.height;
        startTest.textAlign = "center"; //设置水平对齐方式
        startTest.verticalAlign = "middle"; //设置垂直对齐方式
        startTest.text = "开始测算";
        startTest.size = 30;
        startTest.textColor = 0xffffff;
        // 各自回家
        zm_intBox.addChild(zm_intBoxBg);
        zm_intBox.addChild(zm_whenHappen);
        zm_intBox.addChild(zm_title);
        zm_intBox.addChild(zm_shaonian);
        zm_intBox.addChild(zm_linePng);
        zm_intBox.addChild(zm_nameInput);
        zm_backPayBox.addChild(zm_backPayButton);
        zm_backPayBox.addChild(startTest);
        // 客服文本以及图像
        zm_openKeFu.width = stageW;
        var kefu_textOne = new egret.TextField();
        var kefu_textTwo = new egret.TextField();
        kefu_textOne.width = kefu_textTwo.width = stageW;
        kefu_textOne.height = kefu_textTwo.height = 40;
        kefu_textOne.textColor = kefu_textTwo.textColor = 0x000000; //设置颜色
        kefu_textOne.size = kefu_textTwo.size = 28; //设置文本字号
        kefu_textOne.textAlign = kefu_textTwo.textAlign = "center"; //设置水平对齐方式
        kefu_textOne.verticalAlign = kefu_textTwo.verticalAlign = "middle"; //设置垂直对齐方式
        kefu_textOne.y = 20;
        kefu_textTwo.y = kefu_textOne.y + kefu_textOne.height;
        var kefu_image = this.createBitmapByName("kefu_png");
        kefu_image.x = stageW / 2 - 170;
        kefu_image.y = kefu_textTwo.y - kefu_textTwo.height / 4.5;
        kefu_textOne.text = "如果需要帮助，请点此联系客服";
        kefu_textTwo.text = "我们的售后客服";
        var kefu_line = new egret.Shape();
        kefu_line.graphics.lineStyle(2, 0x000000);
        kefu_line.graphics.moveTo((kefu_image.x + kefu_image.width + 20), (kefu_textTwo.y + 33));
        kefu_line.graphics.lineTo(420, (kefu_textTwo.y + 33));
        kefu_line.graphics.endFill();
        // 添加到客服专栏
        zm_openKeFu.addChild(kefu_textOne);
        zm_openKeFu.addChild(kefu_textTwo);
        zm_openKeFu.addChild(kefu_image);
        zm_openKeFu.addChild(kefu_line);
        zm_openKeFu.height = kefu_textTwo.height * 2 + 60;
        // 分布高度
        zm_header.y = 0;
        zm_personNum.y = zm_header.height;
        zm_intBox.y = zm_personNum.y + zm_personNum.height;
        zm_usersThink.y = zm_intBox.y + zm_intBox.height;
        zm_xunAi.y = zm_usersThink.y + zm_usersThink.height;
        zm_dashiContent.y = zm_xunAi.y + zm_xunAi.height;
        zm_calculateBg.y = zm_dashiContent.y + zm_dashiContent.height;
        zm_twoIm.y = zm_calculateBg.y + zm_calculateBg.height;
        zm_backPayBox.y = zm_twoIm.y + zm_twoIm.height + 30;
        zm_usersThinkTwo.y = zm_backPayBox.y + zm_backPayBox.height + 30;
        zm_openKeFu.y = zm_usersThinkTwo.y + zm_usersThinkTwo.height;
        // 确定index_bg和高度值
        index_bg.height = zm_openKeFu.y + zm_openKeFu.height;
        index_bgColor.graphics.beginFill(0xEDC6B0);
        index_bgColor.graphics.drawRect(0, 0, stageW, index_bg.height);
        index_bgColor.graphics.endFill();
        // 需要居中的元素
        zm_header.x = (stageW - zm_header.width) / 2;
        zm_personNum.x = (stageW - zm_personNum.width) / 2;
        zm_intBox.x = (stageW - zm_intBox.width) / 2;
        zm_usersThink.x = (stageW - zm_usersThink.width) / 2;
        zm_xunAi.x = (stageW - zm_xunAi.width) / 2;
        zm_dashiContent.x = (stageW - zm_dashiContent.width) / 2;
        zm_calculateBg.x = (stageW - zm_calculateBg.width) / 2;
        zm_twoIm.x = (stageW - zm_twoIm.width) / 2;
        zm_usersThinkTwo.x = (stageW - zm_usersThinkTwo.width) / 2;
        zm_openKeFu.x = (stageW - zm_openKeFu.width) / 2;
        // 添加入scrollView滚动视图
        indexScrollView.setContent(index_bgColor);
        // 视图内部元素
        index_bgColor.addChild(index_bg);
        index_bgColor.addChild(zm_header);
        index_bgColor.addChild(zm_personNum);
        index_bgColor.addChild(zm_intBox);
        index_bgColor.addChild(zm_usersThink);
        index_bgColor.addChild(zm_xunAi);
        index_bgColor.addChild(zm_dashiContent);
        index_bgColor.addChild(zm_calculateBg);
        index_bgColor.addChild(zm_twoIm);
        index_bgColor.addChild(zm_backPayBox);
        index_bgColor.addChild(zm_usersThinkTwo);
        index_bgColor.addChild(zm_openKeFu);
        // 事件监听
        zm_openKeFu.addEventListener(egret.TouchEvent.TOUCH_TAP, openWxKefu, this);
        startTest.addEventListener(egret.TouchEvent.TOUCH_MOVE, backPayBtnPosition, this);
        function openWxKefu() {
            // 打开微信客服
            console.log("打开客服");
        }
        function backPayBtnPosition() {
            // 返回到开始测算按钮位置
            console.log("返回到开始测算按钮位置");
            var payBtnPosition = zm_backPayBox.y;
            indexScrollView.setScrollPosition(payBtnPosition, 0);
        }
    };
    /**
     * 根据name关键字创建一个Bitmap对象。name属性请参考resources/resource.json配置文件的内容。
     * Create a Bitmap object according to name keyword.As for the property of name please refer to the configuration file of resources/resource.json.
     */
    Main.prototype.createBitmapByName = function (name) {
        var result = new egret.Bitmap();
        var texture = RES.getRes(name);
        result.texture = texture;
        return result;
    };
    /**
     * 描述文件加载成功，开始播放动画
     * Description file loading is successful, start to play the animation
     */
    Main.prototype.startAnimation = function (result) {
        var _this = this;
        var parser = new egret.HtmlTextParser();
        var textflowArr = result.map(function (text) { return parser.parse(text); });
        var textfield = this.textfield;
        var count = -1;
        var change = function () {
            count++;
            if (count >= textflowArr.length) {
                count = 0;
            }
            var textFlow = textflowArr[count];
            // 切换描述内容
            // Switch to described content
            // textfield.textFlow = textFlow;
            var tw = egret.Tween.get(textfield);
            tw.to({ "alpha": 1 }, 200);
            tw.wait(2000);
            tw.to({ "alpha": 0 }, 200);
            tw.call(change, _this);
        };
        change();
    };
    /**
     * 点击按钮
     * Click the button
     */
    Main.prototype.onButtonClick = function (e) {
        var panel = new eui.Panel();
        panel.title = "Title";
        panel.horizontalCenter = 0;
        panel.verticalCenter = 0;
        this.addChild(panel);
    };
    return Main;
}(eui.UILayer));
__reflect(Main.prototype, "Main");
var DebugPlatform = (function () {
    function DebugPlatform() {
    }
    DebugPlatform.prototype.getUserInfo = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, { nickName: "username" }];
            });
        });
    };
    DebugPlatform.prototype.login = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    return DebugPlatform;
}());
__reflect(DebugPlatform.prototype, "DebugPlatform", ["Platform"]);
if (!window.platform) {
    window.platform = new DebugPlatform();
}
//////////////////////////////////////////////////////////////////////////////////////
//
//  Copyright (c) 2014-present, Egret Technology.
//  All rights reserved.
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the Egret nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY EGRET AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
//  OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL EGRET AND CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, DATA,
//  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//////////////////////////////////////////////////////////////////////////////////////
var ThemeAdapter = (function () {
    function ThemeAdapter() {
    }
    /**
     * 解析主题
     * @param url 待解析的主题url
     * @param onSuccess 解析完成回调函数，示例：compFunc(e:egret.Event):void;
     * @param onError 解析失败回调函数，示例：errorFunc():void;
     * @param thisObject 回调的this引用
     */
    ThemeAdapter.prototype.getTheme = function (url, onSuccess, onError, thisObject) {
        var _this = this;
        function onResGet(e) {
            onSuccess.call(thisObject, e);
        }
        function onResError(e) {
            if (e.resItem.url == url) {
                RES.removeEventListener(RES.ResourceEvent.ITEM_LOAD_ERROR, onResError, null);
                onError.call(thisObject);
            }
        }
        if (typeof generateEUI !== 'undefined') {
            egret.callLater(function () {
                onSuccess.call(thisObject, generateEUI);
            }, this);
        }
        else if (typeof generateEUI2 !== 'undefined') {
            RES.getResByUrl("resource/gameEui.json", function (data, url) {
                window["JSONParseClass"]["setData"](data);
                egret.callLater(function () {
                    onSuccess.call(thisObject, generateEUI2);
                }, _this);
            }, this, RES.ResourceItem.TYPE_JSON);
        }
        else {
            RES.addEventListener(RES.ResourceEvent.ITEM_LOAD_ERROR, onResError, null);
            RES.getResByUrl(url, onResGet, this, RES.ResourceItem.TYPE_TEXT);
        }
    };
    return ThemeAdapter;
}());
__reflect(ThemeAdapter.prototype, "ThemeAdapter", ["eui.IThemeAdapter"]);
var LoadingUI = (function (_super) {
    __extends(LoadingUI, _super);
    function LoadingUI() {
        var _this = _super.call(this) || this;
        _this.createView();
        return _this;
    }
    LoadingUI.prototype.createView = function () {
        this.textField = new egret.TextField();
        this.addChild(this.textField);
        this.textField.y = 300;
        this.textField.width = 480;
        this.textField.height = 100;
        this.textField.textAlign = "center";
    };
    LoadingUI.prototype.onProgress = function (current, total) {
        this.textField.text = "Loading..." + current + "/" + total;
    };
    return LoadingUI;
}(egret.Sprite));
__reflect(LoadingUI.prototype, "LoadingUI", ["RES.PromiseTaskReporter"]);
var rotateLeft = function (lValue, iShiftBits) {
    return (lValue << iShiftBits) | (lValue >>> (32 - iShiftBits));
};
var addUnsigned = function (lX, lY) {
    var lX4, lY4, lX8, lY8, lResult;
    lX8 = (lX & 0x80000000);
    lY8 = (lY & 0x80000000);
    lX4 = (lX & 0x40000000);
    lY4 = (lY & 0x40000000);
    lResult = (lX & 0x3FFFFFFF) + (lY & 0x3FFFFFFF);
    if (lX4 & lY4)
        return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
    if (lX4 | lY4) {
        if (lResult & 0x40000000)
            return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
        else
            return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
    }
    else {
        return (lResult ^ lX8 ^ lY8);
    }
};
var F = function (x, y, z) {
    return (x & y) | ((~x) & z);
};
var G = function (x, y, z) {
    return (x & z) | (y & (~z));
};
var H = function (x, y, z) {
    return (x ^ y ^ z);
};
var I = function (x, y, z) {
    return (y ^ (x | (~z)));
};
var FF = function (a, b, c, d, x, s, ac) {
    a = addUnsigned(a, addUnsigned(addUnsigned(F(b, c, d), x), ac));
    return addUnsigned(rotateLeft(a, s), b);
};
var GG = function (a, b, c, d, x, s, ac) {
    a = addUnsigned(a, addUnsigned(addUnsigned(G(b, c, d), x), ac));
    return addUnsigned(rotateLeft(a, s), b);
};
var HH = function (a, b, c, d, x, s, ac) {
    a = addUnsigned(a, addUnsigned(addUnsigned(H(b, c, d), x), ac));
    return addUnsigned(rotateLeft(a, s), b);
};
var II = function (a, b, c, d, x, s, ac) {
    a = addUnsigned(a, addUnsigned(addUnsigned(I(b, c, d), x), ac));
    return addUnsigned(rotateLeft(a, s), b);
};
var convertToWordArray = function (string) {
    var lWordCount;
    var lMessageLength = string.length;
    var lNumberOfWordsTempOne = lMessageLength + 8;
    var lNumberOfWordsTempTwo = (lNumberOfWordsTempOne - (lNumberOfWordsTempOne % 64)) / 64;
    var lNumberOfWords = (lNumberOfWordsTempTwo + 1) * 16;
    var lWordArray = Array(lNumberOfWords - 1);
    var lBytePosition = 0;
    var lByteCount = 0;
    while (lByteCount < lMessageLength) {
        lWordCount = (lByteCount - (lByteCount % 4)) / 4;
        lBytePosition = (lByteCount % 4) * 8;
        lWordArray[lWordCount] = (lWordArray[lWordCount] | (string.charCodeAt(lByteCount) << lBytePosition));
        lByteCount++;
    }
    lWordCount = (lByteCount - (lByteCount % 4)) / 4;
    lBytePosition = (lByteCount % 4) * 8;
    lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80 << lBytePosition);
    lWordArray[lNumberOfWords - 2] = lMessageLength << 3;
    lWordArray[lNumberOfWords - 1] = lMessageLength >>> 29;
    return lWordArray;
};
var wordToHex = function (lValue) {
    var WordToHexValue = "", WordToHexValueTemp = "", lByte, lCount;
    for (lCount = 0; lCount <= 3; lCount++) {
        lByte = (lValue >>> (lCount * 8)) & 255;
        WordToHexValueTemp = "0" + lByte.toString(16);
        WordToHexValue = WordToHexValue + WordToHexValueTemp.substr(WordToHexValueTemp.length - 2, 2);
    }
    return WordToHexValue;
};
var uTF8Encode = function (string) {
    string = string.replace(/\x0d\x0a/g, "\x0a");
    var output = "";
    for (var n = 0; n < string.length; n++) {
        var c = string.charCodeAt(n);
        if (c < 128) {
            output += String.fromCharCode(c);
        }
        else if ((c > 127) && (c < 2048)) {
            output += String.fromCharCode((c >> 6) | 192);
            output += String.fromCharCode((c & 63) | 128);
        }
        else {
            output += String.fromCharCode((c >> 12) | 224);
            output += String.fromCharCode(((c >> 6) & 63) | 128);
            output += String.fromCharCode((c & 63) | 128);
        }
    }
    return output;
};
var MD5 = (function () {
    function MD5() {
    }
    MD5.createNonceStr = function (length) {
        //  默认创建16为随机字符串
        length || (length = 16);
        var alphabet = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        var result = '';
        for (var i = 0; i < length; i++) {
            result += alphabet[Math.floor(Math.random() * (62))];
        }
        return result;
    };
    MD5.createSignObject = function (param) {
        var param_str = '', noncestr = this.createNonceStr();
        param['nonce_str'] = noncestr;
        var param_arr = [];
        for (var key in param) {
            param_arr.push(key + '=' + param[key]);
        }
        param_str = param_arr.sort(function (a, b) { return a > b ? 1 : -1; }).join('&') + '&key=' + GameConfig.getKey();
        console.log(param_str);
        param['nonce_str'] = noncestr;
        param['sign'] = this.encode(param_str).toUpperCase();
        return param;
    };
    MD5.encode = function (string) {
        var x = Array();
        var k, AA, BB, CC, DD, a, b, c, d;
        var S11 = 7, S12 = 12, S13 = 17, S14 = 22;
        var S21 = 5, S22 = 9, S23 = 14, S24 = 20;
        var S31 = 4, S32 = 11, S33 = 16, S34 = 23;
        var S41 = 6, S42 = 10, S43 = 15, S44 = 21;
        string = uTF8Encode(string);
        x = convertToWordArray(string);
        a = 0x67452301;
        b = 0xEFCDAB89;
        c = 0x98BADCFE;
        d = 0x10325476;
        for (k = 0; k < x.length; k += 16) {
            AA = a;
            BB = b;
            CC = c;
            DD = d;
            a = FF(a, b, c, d, x[k + 0], S11, 0xD76AA478);
            d = FF(d, a, b, c, x[k + 1], S12, 0xE8C7B756);
            c = FF(c, d, a, b, x[k + 2], S13, 0x242070DB);
            b = FF(b, c, d, a, x[k + 3], S14, 0xC1BDCEEE);
            a = FF(a, b, c, d, x[k + 4], S11, 0xF57C0FAF);
            d = FF(d, a, b, c, x[k + 5], S12, 0x4787C62A);
            c = FF(c, d, a, b, x[k + 6], S13, 0xA8304613);
            b = FF(b, c, d, a, x[k + 7], S14, 0xFD469501);
            a = FF(a, b, c, d, x[k + 8], S11, 0x698098D8);
            d = FF(d, a, b, c, x[k + 9], S12, 0x8B44F7AF);
            c = FF(c, d, a, b, x[k + 10], S13, 0xFFFF5BB1);
            b = FF(b, c, d, a, x[k + 11], S14, 0x895CD7BE);
            a = FF(a, b, c, d, x[k + 12], S11, 0x6B901122);
            d = FF(d, a, b, c, x[k + 13], S12, 0xFD987193);
            c = FF(c, d, a, b, x[k + 14], S13, 0xA679438E);
            b = FF(b, c, d, a, x[k + 15], S14, 0x49B40821);
            a = GG(a, b, c, d, x[k + 1], S21, 0xF61E2562);
            d = GG(d, a, b, c, x[k + 6], S22, 0xC040B340);
            c = GG(c, d, a, b, x[k + 11], S23, 0x265E5A51);
            b = GG(b, c, d, a, x[k + 0], S24, 0xE9B6C7AA);
            a = GG(a, b, c, d, x[k + 5], S21, 0xD62F105D);
            d = GG(d, a, b, c, x[k + 10], S22, 0x2441453);
            c = GG(c, d, a, b, x[k + 15], S23, 0xD8A1E681);
            b = GG(b, c, d, a, x[k + 4], S24, 0xE7D3FBC8);
            a = GG(a, b, c, d, x[k + 9], S21, 0x21E1CDE6);
            d = GG(d, a, b, c, x[k + 14], S22, 0xC33707D6);
            c = GG(c, d, a, b, x[k + 3], S23, 0xF4D50D87);
            b = GG(b, c, d, a, x[k + 8], S24, 0x455A14ED);
            a = GG(a, b, c, d, x[k + 13], S21, 0xA9E3E905);
            d = GG(d, a, b, c, x[k + 2], S22, 0xFCEFA3F8);
            c = GG(c, d, a, b, x[k + 7], S23, 0x676F02D9);
            b = GG(b, c, d, a, x[k + 12], S24, 0x8D2A4C8A);
            a = HH(a, b, c, d, x[k + 5], S31, 0xFFFA3942);
            d = HH(d, a, b, c, x[k + 8], S32, 0x8771F681);
            c = HH(c, d, a, b, x[k + 11], S33, 0x6D9D6122);
            b = HH(b, c, d, a, x[k + 14], S34, 0xFDE5380C);
            a = HH(a, b, c, d, x[k + 1], S31, 0xA4BEEA44);
            d = HH(d, a, b, c, x[k + 4], S32, 0x4BDECFA9);
            c = HH(c, d, a, b, x[k + 7], S33, 0xF6BB4B60);
            b = HH(b, c, d, a, x[k + 10], S34, 0xBEBFBC70);
            a = HH(a, b, c, d, x[k + 13], S31, 0x289B7EC6);
            d = HH(d, a, b, c, x[k + 0], S32, 0xEAA127FA);
            c = HH(c, d, a, b, x[k + 3], S33, 0xD4EF3085);
            b = HH(b, c, d, a, x[k + 6], S34, 0x4881D05);
            a = HH(a, b, c, d, x[k + 9], S31, 0xD9D4D039);
            d = HH(d, a, b, c, x[k + 12], S32, 0xE6DB99E5);
            c = HH(c, d, a, b, x[k + 15], S33, 0x1FA27CF8);
            b = HH(b, c, d, a, x[k + 2], S34, 0xC4AC5665);
            a = II(a, b, c, d, x[k + 0], S41, 0xF4292244);
            d = II(d, a, b, c, x[k + 7], S42, 0x432AFF97);
            c = II(c, d, a, b, x[k + 14], S43, 0xAB9423A7);
            b = II(b, c, d, a, x[k + 5], S44, 0xFC93A039);
            a = II(a, b, c, d, x[k + 12], S41, 0x655B59C3);
            d = II(d, a, b, c, x[k + 3], S42, 0x8F0CCC92);
            c = II(c, d, a, b, x[k + 10], S43, 0xFFEFF47D);
            b = II(b, c, d, a, x[k + 1], S44, 0x85845DD1);
            a = II(a, b, c, d, x[k + 8], S41, 0x6FA87E4F);
            d = II(d, a, b, c, x[k + 15], S42, 0xFE2CE6E0);
            c = II(c, d, a, b, x[k + 6], S43, 0xA3014314);
            b = II(b, c, d, a, x[k + 13], S44, 0x4E0811A1);
            a = II(a, b, c, d, x[k + 4], S41, 0xF7537E82);
            d = II(d, a, b, c, x[k + 11], S42, 0xBD3AF235);
            c = II(c, d, a, b, x[k + 2], S43, 0x2AD7D2BB);
            b = II(b, c, d, a, x[k + 9], S44, 0xEB86D391);
            a = addUnsigned(a, AA);
            b = addUnsigned(b, BB);
            c = addUnsigned(c, CC);
            d = addUnsigned(d, DD);
        }
        var tempValue = wordToHex(a) + wordToHex(b) + wordToHex(c) + wordToHex(d);
        return tempValue.toLowerCase();
    };
    return MD5;
}());
__reflect(MD5.prototype, "MD5");
var TimePicker = (function () {
    function TimePicker() {
        this.start();
    }
    TimePicker.prototype.start = function () {
        var yearScroll = new eui.Scroller(); // 年滚动
        var monthScroll = new eui.Scroller(); // 月滚动
        var dayScroll = new eui.Scroller(); // 日滚动
        var hourScroll = new eui.Scroller(); // 时滚动
        var screenWidth = GameConfig.getWidth();
        // 四分天下
        yearScroll.width = monthScroll.width = dayScroll.width = hourScroll.width = screenWidth / 4;
        // 高
        yearScroll.height = monthScroll.height = dayScroll.height = hourScroll.height = 60;
        // y轴
        yearScroll.y = monthScroll.y = dayScroll.y = hourScroll.y = 0;
        // x轴
        yearScroll.x = screenWidth / 4 * 0;
        monthScroll.x = screenWidth / 4 * 1;
        dayScroll.x = screenWidth / 2 * 2;
        hourScroll.x = screenWidth / 2 * 3;
        // 滑动元素
        var yearList = new eui.List();
        var monthList = new eui.List();
        var dayList = new eui.List();
        var hourList = new eui.List();
        // 对用的数组
        var year = [];
        var month = [];
        var day = [];
        var hour = ['不确定时'];
        for (var i = 1900; i <= new Date().getFullYear(); i++) {
            year.push(i);
        }
        for (var i = 1; i <= 12; i++) {
            month.push(i);
        }
        for (var i = 1; i <= 31; i++) {
            day.push(i);
        }
        for (var i = 1; i <= 31; i++) {
            hour.push(i);
        }
        // 创建ArrayCollection实例
        yearList.dataProvider = new eui.ArrayCollection(year);
        monthList.dataProvider = new eui.ArrayCollection(month);
        dayList.dataProvider = new eui.ArrayCollection(day);
        hourList.dataProvider = new eui.ArrayCollection(hour);
        // 滚动视口绑定
        yearScroll.viewport = yearList;
        dayScroll.viewport = monthList;
        monthScroll.viewport = dayList;
        hourScroll.viewport = hourList;
        yearScroll.addEventListener(eui.UIEvent.CHANGE_END, moveScroller, this);
        monthScroll.addEventListener(eui.UIEvent.CHANGE_END, moveScroller, this);
        dayScroll.addEventListener(eui.UIEvent.CHANGE_END, moveScroller, this);
        hourScroll.addEventListener(eui.UIEvent.CHANGE_END, moveScroller, this);
        yearScroll.bounces = false;
        monthScroll.bounces = false;
        dayScroll.bounces = false;
        var scrollBg = new egret.Sprite();
        scrollBg.y = 400;
        scrollBg.addChild(yearScroll);
        scrollBg.addChild(monthScroll);
        scrollBg.addChild(dayScroll);
        scrollBg.addChild(hourScroll);
        GameConfig.getMain().addChild(scrollBg);
        var birth = '';
        function moveScroller() {
            if ((yearScroll.viewport.scrollV + yearScroll.height) >= yearScroll.viewport.contentHeight || yearScroll.viewport.scrollV < 0) {
                console.log("滚动到底部了");
                birth += year[year.length - 1];
            }
            else {
                var num = Math.floor(yearScroll.viewport.scrollV / 50);
                if (yearScroll.viewport.scrollV % 50 > 30) {
                    num++;
                }
                birth += year[num];
                yearScroll.viewport.scrollV = num * 50;
            }
            if ((monthScroll.viewport.scrollV + monthScroll.height) >= monthScroll.viewport.contentHeight || monthScroll.viewport.scrollV < 0) {
                console.log("滚动到底部了");
                birth += month[month.length - 1];
            }
            else {
                var num = Math.floor(monthScroll.viewport.scrollV / 50);
                if (monthScroll.viewport.scrollV % 50 > 30) {
                    num++;
                }
                birth += month[num];
                monthScroll.viewport.scrollV = num * 50;
            }
            if ((dayScroll.viewport.scrollV + dayScroll.height) >= dayScroll.viewport.contentHeight || dayScroll.viewport.scrollV < 0) {
                console.log("滚动到底部了");
                birth += day[day.length - 1];
            }
            else {
                var num = Math.floor(dayScroll.viewport.scrollV / 50);
                if (dayScroll.viewport.scrollV % 50 > 30) {
                    num++;
                }
                birth += day[num];
                dayScroll.viewport.scrollV = num * 50;
            }
            console.log(birth);
            birth = "";
        }
    };
    return TimePicker;
}());
__reflect(TimePicker.prototype, "TimePicker");
var Utils = (function () {
    function Utils() {
    }
    Utils.getNowDate = function () {
        return this.dateFtt(new Date(), "yyyy-MM-dd hh:mm:ss");
    };
    Utils.getWeekTime = function () {
        var now = new Date();
        var y = now.getFullYear();
        var m = now.getMonth();
        var d = now.getDate();
        var day = now.getDay();
        var weekStart = new Date(y, m, d - (day ? (day - 1) : 6));
        var weekEnd = new Date(y, m, d + (day ? (8 - day) : 1));
        Utils.dateRange = [this.dateFtt(weekStart), this.dateFtt(weekEnd)];
        return Utils.dateRange;
    };
    Utils.isInTimeRange = function (timeStr) {
        if (timeStr) {
            var recordTime = new Date(timeStr.replace(/-/g, '/')).getTime();
            return recordTime > this.dateRange[0] && recordTime < this.dateRange[1];
        }
        else {
            return false;
        }
    };
    Utils.transObj = function (kvData) {
        var res = {};
        kvData.map(function (data) {
            res[data.key] = data.value;
        });
        return res;
    };
    // 时间格式处理
    Utils.dateFtt = function (date, fmt) {
        var o = {
            "M+": date.getMonth() + 1,
            "d+": date.getDate(),
            "h+": date.getHours(),
            "m+": date.getMinutes(),
            "s+": date.getSeconds(),
            "q+": Math.floor((date.getMonth() + 3) / 3),
            "S": date.getMilliseconds() //毫秒   
        };
        fmt || (fmt = "yyyy-MM-ddThh:mm:ss+00:00");
        if (/(y+)/.test(fmt))
            fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o)
            if (new RegExp("(" + k + ")").test(fmt))
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    };
    Utils.dateRange = [];
    return Utils;
}());
__reflect(Utils.prototype, "Utils");
var AssetAdapter = (function () {
    function AssetAdapter() {
    }
    /**
     * @language zh_CN
     * 解析素材
     * @param source 待解析的新素材标识符
     * @param compFunc 解析完成回调函数，示例：callBack(content:any,source:string):void;
     * @param thisObject callBack的 this 引用
     */
    AssetAdapter.prototype.getAsset = function (source, compFunc, thisObject) {
        function onGetRes(data) {
            compFunc.call(thisObject, data, source);
        }
        if (RES.hasRes(source)) {
            var data = RES.getRes(source);
            if (data) {
                onGetRes(data);
            }
            else {
                RES.getResAsync(source, onGetRes, this);
            }
        }
        else {
            RES.getResByUrl(source, onGetRes, this, RES.ResourceItem.TYPE_IMAGE);
        }
    };
    return AssetAdapter;
}());
__reflect(AssetAdapter.prototype, "AssetAdapter", ["eui.IAssetAdapter"]);
// 版本控制组件
var VersionCtrl = (function () {
    function VersionCtrl() {
    }
    /**
     * 调用refreshVersionCtrl重新获取
     */
    VersionCtrl.refreshVersionCtrl = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, Api.getConfiguration()];
                    case 1:
                        _a.configuration = _b.sent();
                        console.log(this.configuration);
                        return [2 /*return*/];
                }
            });
        });
    };
    VersionCtrl.queryConfig = function (key) {
        return this.configuration[key];
    };
    return VersionCtrl;
}());
__reflect(VersionCtrl.prototype, "VersionCtrl");
;window.Main = Main;