let common = require("Common");
let _num = 0; //点击后的卡片下标
let btnClick = false; // 点击间隔

cc.Class({
    extends: cc.Component,

    properties: {
        back: cc.Sprite,
        music: cc.Sprite,
        countLabel: cc.Label,
        // 四个mask
        first: cc.Sprite,
        second: cc.Sprite,
        third: cc.Sprite,
        fourth: cc.Sprite,
        // 三个大按钮
        next: cc.Sprite,
        prev: cc.Sprite,
        nextBtn: cc.Label,
        prevBtn: cc.Label,
        cardBox: cc.Node,
        finish: cc.Sprite,
        finishBtn: cc.Label,
        finishBox: cc.Sprite,
        // z最后的卡片
        conImg: cc.Sprite,
        saveImg: cc.Sprite,
        shareImg: cc.Sprite,
        // shadow
        s1: cc.Sprite,
        s2: cc.Sprite,
        s3: cc.Sprite,
        s4: cc.Sprite,
        foText: cc.Label,
        // fobidden
        f1: cc.Sprite,
        // whiteBg
        wb1: cc.Sprite,
        wb2: cc.Sprite,
        wb3: cc.Sprite,
        wb4: cc.Sprite,
        cover: cc.Sprite,
        //dialog
        dialog: cc.Sprite
    },
    onLoad () {
        let that = this
        // 加载默认样式
        this.btnLoad()
        this.back.node.on(cc.Node.EventType.TOUCH_END, this.backMain, this)
        // 点击上/下部分切换
        this.next.node.on(cc.Node.EventType.TOUCH_START, this.changeNextMask, this)
        this.prev.node.on(cc.Node.EventType.TOUCH_START, this.changePrevMask, this)
        this.finish.node.on(cc.Node.EventType.TOUCH_START, this.finishClean, this)

        this.conImg.node.on(cc.Node.EventType.TOUCH_START, this.refreshScene, this)
        this.shareImg.node.on(cc.Node.EventType.TOUCH_START, this.shareOther, this)
        this.saveImg.node.on(cc.Node.EventType.TOUCH_START, this.saveImage2local, this)
        this.music.node.on(cc.Node.EventType.TOUCH_START, this.goMusicScene, this)
        this.f1.node.on(cc.Node.EventType.TOUCH_START, this.fobiddenEvent, this)

        this.cover.node.on(cc.Node.EventType.TOUCH_START, this.fobiddenEvent, this)
        this.cover.node.on(cc.Node.EventType.TOUCH_MOVE, this.fobiddenEvent, this)
        
        if(!common.isPlay){
            cc.loader.load('https://image.taotaoxi.cn/minigame/music/0.mp3', function(err, clip) {
                let audioId = cc.audioEngine.play(clip, true);
                that.music.node.runAction(cc.repeatForever(cc.rotateBy(1.0, 360)))
                common.setStorage('cm', '0.mp3', function(){
                    common.isPlay = true
                    common.setStorage('audioId', audioId)
                })
            });
        }else{
            let state = cc.audioEngine.getState(common.getStorage('audioId'))
            if(state == 1){
                that.music.node.runAction(cc.repeatForever(cc.rotateBy(1.0, 360)))
            }else{
                cc.audioEngine.resume(common.getStorage('audioId'));
                that.music.node.runAction(cc.repeatForever(cc.rotateBy(1.0, 360)))
            }
        }
    },
    fobiddenEvent: function(){
        return false;
    },
    backMain: function(){
        this.node.runAction(cc.sequence(cc.fadeOut(0.1),cc.callFunc(function(){
            cc.audioEngine.pauseAllEffects()
            cc.director.loadScene('Detail')
        })));
    },
    goMusicScene: function(){
        // this.node.runAction(cc.sequence(cc.fadeOut(0.1),cc.callFunc(function(){
        //     cc.director.loadScene('ChooseMusic')
        // })));
        cc.director.loadScene("ChooseMusic",(err,scene)=>{
            cc.sequence(cc.fadeOut(0.1),cc.callFunc(function(){
                cc.director.loadScene('ChooseMusic')
            }))
        })
    },
    changePrevMask: function (params) {
        if(!btnClick) {return false;}
        if(_num > 0){
            this.prev.node.active = true;
            this.next.node.active = true;
            this.finish.node.active = false;
            this.prev.node.x = -250
            this.next.node.x = 250
            --_num;
            this.tabFourMask(1)
            if(_num == 0){
                this.prev.node.active = false;
                this.next.node.active = true;           
                this.next.node.x = 0
                _num = 0
            }
        }
        btnClick = false;
    },
    changeNextMask: function(){
        if(!btnClick) {return false;}      
        if(_num < 3){
            this.prev.node.active = true;
            this.next.node.active = true;
            this.next.node.x = 250
            this.prev.node.x = -250
            _num++;
            this.tabFourMask(-1)
            if(_num == 3){
                this.next.node.active = false;
                this.prev.node.active = true;
                this.finish.node.active = true;
                this.prev.node.x = -250
                this.finish.node.x = 250
            }
        }
        btnClick = false;
    },
    btnLoad: function(){
        // common.playMusic('0.mp3')
        // this.music.node.runAction(cc.repeatForever(cc.rotateBy(1.0, 360)))
        // if(!cc.audioEngine.isMusicPlaying()){
            
        // }
        btnClick = true;
        _num = 0
        this.cardBox.x = 0
        this.countLabel.node.color = new cc.color(22,100,45,255);
        this.foText.node.color = new cc.color(0,0,0,255);
        this.nextBtn.node.color = new cc.color(0,0,0,255);
        this.prevBtn.node.color = new cc.color(0,0,0,255);
        this.finishBtn.node.color = new cc.color(0,0,0,255);
        this.countLabelUpdate()
        // 上下部分按钮
        this.prev.node.active = false
        this.finish.node.active = false
        this.next.node.x = 0

        this.foText.string = common.foText[Math.floor(Math.random() * common.foText.length)]
    },
    tabFourMask: function(n){
        // (n > 0) ? (this.cardBox.x += 300) : (this.cardBox.x -= 300)
        if(n > 0){
            this.cardBox.runAction(cc.sequence(cc.moveBy(0.5, cc.p(1100, 0)), cc.callFunc(function(){
                btnClick = true;
            }))); 
        }else{
            this.cardBox.runAction(cc.sequence(cc.moveBy(0.5, cc.p(-1100, 0)), cc.callFunc(function(){
                btnClick = true;
            }))); 
        }
    },
    finishClean: function() {
        if(!btnClick) {return false;}
        this.cardBox.x = -4300
        let that = this
        this.wb1.node.active = false
        this.wb2.node.active = false
        this.wb3.node.active = false
        this.wb4.node.active = false

        this.cover.node.x = 510
        this.cover.node.y = 1140

        

        that.finishBox.node.runAction(cc.sequence(cc.scaleBy(0.5, 10),cc.callFunc(function(){
            that.prev.node.active = false
            that.finish.node.active = false
            common.setCurrent()
            btnClick = true;
            that.fourCardAnimation(that)
        })));  

        btnClick = false;
    },
    countLabelUpdate: function () {
        this.countLabel.string = common.countNum()
    },
    refreshScene: function () {
        // cc.director.loadScene("DrawFo")
        let that = this
        this.node.runAction(cc.sequence(cc.fadeOut(0.1),cc.callFunc(function(){
            cc.director.loadScene('cleanMask')
            that.countLabelUpdate()
        })));
        
    },
    saveImage2local: function () {
        let that = this
        that.createImageObj(120, 300, 830, 1300, 830, 1300,function(_res){
            // FileSystemManager.saveFile({
            //     tempFilePath: _res
            // })
            wx.getSetting({
                success: res => {
                    if (!res.authSetting['scope.writePhotosAlbum']) {
                        wx.authorize({
                            scope: 'scope.writePhotosAlbum',
                            success() {
                                saveImgHaveScope(_res, () =>{
                                    console.log(1)
                                })
                            }, fail: function () {
                                wx.openSetting({
                                    success: (res) => {
                                        if (res.authSetting["scope.writePhotosAlbum"] === true) {
                                            saveImgHaveScope(_res, () =>{
                                                console.log(2)
                                            })
                                        } else {
                                            setTimeout(function () {
                                                if (res.authSetting["scope.writePhotosAlbum"] === true) {
                                                    saveImgHaveScope(_res, () =>{
                                                        console.log(3)
                                                    })
                                                }
                                            }, 200)
                                        }
                                    }
                                })
                            }
                        })
                    } else {
                        saveImgHaveScope(_res, () =>{
                            console.log(4)
                        })
                    }
                }
            })
            function saveImgHaveScope(imgUrl, fn){
                wx.saveImageToPhotosAlbum({
                    filePath: imgUrl,
                    success: r => {
                        fn()
                    }
                })
            }
        })
    },
    shareOther: function () {
        let that = this
        // that.createImageObj(0, 0, 1080, 1920, 1080, 1920,function(_res){
        //     wx.shareAppMessage({
        //         title: "手绘佛",
        //         imageUrl: _res.tempFilePath,
        //         success: _r => {
        //             console.log(_r)
        //             that.countLabelUpdate()
        //         },
        //         fail: err => {
        //             that.countLabelUpdate()
        //         }
        //     })
        // })
        wx.shareAppMessage({
            title: "一花一世界，一佛一如来。一起画佛静心吧！",
            imageUrl: "https://image.taotaoxi.cn/minigame/music/share.png",
            success: _r => {
                that.countLabelUpdate()
            },
            fail: err => {
                that.countLabelUpdate()
            }
        })
    },
    onShow: function(){
        cc.audioEngine.resumeAllEffects()
    },
    fourCardAnimation: function(_this){
        _this.s1.node.active = false
        _this.s2.node.active = false
        _this.s3.node.active = false
        _this.s4.node.active = false
        _this.first.node.runAction(cc.spawn(cc.moveTo(0, cc.p(4129, 410)), cc.scaleTo(0, 0.42)))
        _this.second.node.runAction(cc.spawn(cc.moveTo(0, cc.p(4471, 410)), cc.scaleTo(0, 0.42)))
        _this.third.node.runAction(cc.spawn(cc.moveTo(0, cc.p(4129, -35)), cc.scaleTo(0, 0.42)))
        _this.fourth.node.runAction(cc.spawn(cc.moveTo(0, cc.p(4471, -35)), cc.scaleTo(0, 0.42)))
    },
    createImageObj: function(x, y, w, h, dw, dh, fn){
        var canvas = cc.game.canvas;
        canvas.toTempFilePath({
            x: x,
            y: y,
            width: w,
            height: h,
            destWidth: dw,
            destHeight: dh,
            success (res) {
                fn(res.tempFilePath)
            }
        })
    },
    start () {
        if(cc.sys.os === cc.sys.OS_ANDROID||cc.sys.os==cc.sys.OS_IOS){
            cc.view.setDesignResolutionSize(1080, 1920, cc.ResolutionPolicy.FIXED_HEIGHT);
        }else{
            cc.view.setDesignResolutionSize(1080, 1920, cc.ResolutionPolicy.SHOW_ALL);
        }
    }
});
// this.bottomImg.spriteFrame.setTexture(cc.url.raw(common.bottomImgArr[_num]));
// this.maskImg.spriteFrame.setTexture(cc.url.raw(common.maskImgArr[_num]));
