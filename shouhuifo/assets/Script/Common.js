module.exports = {
    isFirstOpen: isFirstOpen(),
    countNum: countNum,
    setCurrent: setCurrent,
    getStorage: getStorage,
    setStorage: setStorage,
    isPlay: false,
    foText: [
        "一花一世界，一佛一如来。",
        "苦海无涯，回头是岸。",
        "一花一世界，一佛一如来。",
        "苦海无涯，回头是岸。",
        "放下屠刀，立地成佛。",
        "由爱故生忧，由爱故生怖，若离于爱者，无忧亦无怖。",
        "菩提本无树，明镜亦非台，本来无一物，何处惹尘埃。",
        "诸行无常，一切皆苦。诸法无我，寂灭为乐。",
        "欲知前世因，今生受者是；欲知来世果，今生作者是。",
        "救人一命，胜造七级浮屠",
        "过去心不可得此刻心不可得未来心不可得。",
        "心生种种法生，心灭种种法灭。",
        "智人除心不除境，愚人除境不除心。心既除矣，境岂实有。",
        "若人知心行，普造诸世间。是人则见佛，了佛真实性。",
        "得生与否，全由信愿之有无；品位高下，全由持名之深浅。",
        "一念愚即般若绝，一念智即般若生。",
        "一花一世界，一叶一菩提。",
        "若人欲了知，三世一切佛。应观法界性，一切唯心造。",
        "菩提并无树，明镜亦无台，世本无一物，何处染尘埃。",
        "施主一粒米，重如须弥山，今生不了道，披毛戴角还。",
        "一切有为法，如梦幻泡影，如露亦如电，应作如是观。",
        "人之所以痛苦，在于追求错误的东西。",
        "认识自己，降伏自己，改变自己，才能改变别人。",
        "你可以拥有爱，但不要执著，因为分离是必然的。",
        "你什么时候放下，什么时候就没有烦恼。",
        "内心没有分别心，就是真正的苦行。",
        "医生难医命终之人，佛陀难渡无缘的众生。",
        "创造机会的人是勇者。等待机会的人是愚者。",
        "修行要有耐性，要能甘于淡泊，乐于寂寞。",
        "随缘不是得过且过，因循苟且，而是尽人事听天命。",
        "原谅别人，就是给自己心中留下空间，以便回旋。",
        "拥有一颗无私的爱心，便拥有了一切。"
    ]
};

function isFirstOpen() {
    let currentNum = cc.sys.localStorage.getItem("currentNum");
    if(currentNum == null || currentNum == undefined || currentNum == ""){
        return true
    }else{
        return false
    }
}

function countNum() {
    return getCurrentCount()
}

function getCurrentCount(){
    let currentNum = cc.sys.localStorage.getItem("currentNum");
    let reVal = void 0;
    if(currentNum == null || currentNum == undefined || currentNum == ''){
        setStorage('currentNum', 0)
        reVal = "000"
        return reVal
    }else{
        if(currentNum > 99){
            reVal = currentNum
            return reVal
        }else if(currentNum > 9 && currentNum <= 99){
            reVal = "0" + currentNum
            return reVal
        }else{
            reVal = "00" + currentNum
            return reVal
        }
    }
}
function getStorage(key){
    let val = cc.sys.localStorage.getItem(key);
    return val
}
function setStorage(key, val, fn){
    cc.sys.localStorage.setItem(key, val)
    if(arguments.length > 2){
        fn()
    }
}
function setCurrent() {
    let currentCount = getStorage('currentNum');
    if(currentCount == null || currentCount == undefined || currentCount == ''){
        setStorage("currentNum", 1)
    }else{
        setStorage("currentNum", +currentCount + 1)
    }
}