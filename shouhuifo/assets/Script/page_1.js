cc.Class({
    extends: cc.Component,

    properties: {
        pageView: cc.PageView,
        page_1: cc.Sprite,
        page_2: cc.Sprite,
        page_3: cc.Sprite,
        page_4: cc.Sprite
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.page_1.node.on(cc.Node.EventType.TOUCH_END, this.loadOneMask, this);
        // this.page_2.node.on(cc.Node.EventType.TOUCH_END, this.loadTwoMask, this);
        // this.page_3.node.on(cc.Node.EventType.TOUCH_END, this.loadThreeMask, this);
        // this.page_4.node.on(cc.Node.EventType.TOUCH_END, this.loadFourMask, this);
    },
    loadOneMask: function(event){
        this.node.runAction(cc.sequence(cc.fadeOut(0.1),cc.callFunc(function(){
            cc.director.loadScene('Detail')
        })));
    },
    loadTwoMask: function(event){
        this.node.runAction(cc.sequence(cc.fadeOut(0.1),cc.callFunc(function(){
            cc.director.loadScene('cleanMask')
        })));
    },
    loadThreeMask: function(event){
        this.node.runAction(cc.sequence(cc.fadeOut(0.1),cc.callFunc(function(){
            cc.director.loadScene('cleanMask')
        })));
    },
    loadFourMask: function(event){
        this.node.runAction(cc.sequence(cc.fadeOut(0.1),cc.callFunc(function(){
            cc.director.loadScene('clean')
        })));
    },
    start () {
        if(cc.sys.os === cc.sys.OS_ANDROID||cc.sys.os==cc.sys.OS_IOS){
            cc.view.setDesignResolutionSize(1080, 1920, cc.ResolutionPolicy.FIXED_HEIGHT);
        }else{
            cc.view.setDesignResolutionSize(1080, 1920, cc.ResolutionPolicy.SHOW_ALL);
        }
    }
});
