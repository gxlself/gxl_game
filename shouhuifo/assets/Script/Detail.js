cc.Class({
    extends: cc.Component,

    properties: {
        back: cc.Sprite,
        startPaint: cc.Sprite,        
        paint: cc.Label
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.paint.node.color = new cc.color(0,0,0,255);
        this.back.node.on(cc.Node.EventType.TOUCH_END, this.backMain, this)
        this.startPaint.node.on(cc.Node.EventType.TOUCH_END, this.navToClean, this)
    },
    backMain: function(){
        this.node.runAction(cc.sequence(cc.fadeOut(0.1),cc.callFunc(function(){
            cc.director.loadScene('DrawFo')
        })));
    },
    navToClean: function(){
        this.node.runAction(cc.sequence(cc.fadeOut(0.1),cc.callFunc(function(){
            cc.director.loadScene('cleanMask')
        })));
    },
    start () {
        if(cc.sys.os === cc.sys.OS_ANDROID||cc.sys.os==cc.sys.OS_IOS){
            cc.view.setDesignResolutionSize(1080, 1920, cc.ResolutionPolicy.FIXED_HEIGHT);
        }else{
            cc.view.setDesignResolutionSize(1080, 1920, cc.ResolutionPolicy.SHOW_ALL);
        }
    }

    // update (dt) {},
});
