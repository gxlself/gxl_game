cc.Class({
    extends: cc.Component,

    properties: {
        load: cc.Sprite
    },
    onLoad () {
        let that = this
        that.load.node.x = 0
        that.load.node.runAction(cc.sequence(cc.scaleTo(1, 50.5, 1),cc.callFunc(function(){
            // cc.director.loadScene("ChooseMusic",(err,scene)=>{
            //     cc.director.loadScene("ModalCard",(err,scene)=>{
            //         cc.director.loadScene("DrawFo",(err,scene)=>{
            //             cc.sequence(cc.fadeOut(0.1),cc.callFunc( () => {
            //                 cc.director.loadScene('DrawFo')
            //             }))
            //         })
            //     })
            // })
            cc.director.loadScene("DrawFo",(err,scene)=>{
                cc.loader.load('https://image.taotaoxi.cn/minigame/music/0.mp3', function(err, clip) {
                    cc.loader.load('https://image.taotaoxi.cn/minigame/music/1.mp3', function(err, clip) {
                        cc.loader.load('https://image.taotaoxi.cn/minigame/music/2.mp3', function(err, clip) {
                            cc.loader.load('https://image.taotaoxi.cn/minigame/music/3.mp3', function(err, clip) {
                                that.load.node.runAction(cc.scaleTo(10, 10, 1),cc.callFunc(function(){
                                    cc.sequence(cc.fadeOut(0.1),cc.callFunc(function(){
                                        cc.director.loadScene('DrawFo')
                                    }))
                                }))
                            });
                        });
                    });
                });
            })
        })))
    },
    start () {
        if(cc.sys.os === cc.sys.OS_ANDROID||cc.sys.os==cc.sys.OS_IOS){
            cc.view.setDesignResolutionSize(1080, 1920, cc.ResolutionPolicy.FIXED_HEIGHT);
        }else{
            cc.view.setDesignResolutionSize(1080, 1920, cc.ResolutionPolicy.SHOW_ALL);
        }
    },

    // update (dt) {},
});
