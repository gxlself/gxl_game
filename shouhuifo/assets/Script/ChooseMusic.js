let common = require("Common");
cc.Class({
    extends: cc.Component,

    properties: {
       back: cc.Sprite,
       currentMusicShow: cc.Label,
       m1: cc.Sprite,
       m2: cc.Sprite,
       m3: cc.Sprite,
       m4: cc.Sprite,
       s1: cc.Sprite,
       s2: cc.Sprite,
       s3: cc.Sprite,
       s4: cc.Sprite,
    },
    onLoad () {
        this.preMusicList()
        // this.s1.node.active = true
        // this.currentMusicShow.string = '大悲咒'
        let currentMusic = common.getStorage('cm')
        switch(currentMusic){
            case '0.mp3': 
                this.s1.node.active = true;
                this.currentMusicShow.string = '大悲咒';
                break;
            case '1.mp3': 
                this.s2.node.active = true;
                this.currentMusicShow.string = '清溪潺流';
                break;
            case '2.mp3': 
                this.s3.node.active = true;
                this.currentMusicShow.string = '心经';
                break;
            case '3.mp3': 
                this.s4.node.active = true;
                this.currentMusicShow.string = '消灾吉祥神咒';
                break;
            default: 
                this.s1.node.active = true;
                this.currentMusicShow.string = '大悲咒';
                break;
        }
        this.back.node.on(cc.Node.EventType.TOUCH_END, this.backMain, this)
        this.m1.node.on(cc.Node.EventType.TOUCH_START, this.m1play, this)
        this.m2.node.on(cc.Node.EventType.TOUCH_START, this.m2play, this)
        this.m3.node.on(cc.Node.EventType.TOUCH_START, this.m3play, this)
        this.m4.node.on(cc.Node.EventType.TOUCH_START, this.m4play, this)
    },
    backMain: function(){
        this.node.runAction(cc.sequence(cc.fadeOut(0.1),cc.callFunc(function(){
            cc.director.loadScene('ModalCard')
        })));
    },
    preMusicList: function(){
       // let that = this
        this.s1.node.active = false
        this.s2.node.active = false
        this.s3.node.active = false
        this.s4.node.active = false
    },
    m1play: function(){
        this.preMusicList()
        cc.audioEngine.pauseAllEffects()
        this.s1.node.active = true
        this.currentMusicShow.string = '大悲咒'
        cc.loader.load('https://image.taotaoxi.cn/minigame/music/0.mp3', function(err, clip) {
            cc.audioEngine.pause(common.getStorage('audioId'));
            let _audioId = cc.audioEngine.play(clip, true);
            common.setStorage('audioId', _audioId, ()=>{
                common.setStorage('cm', '0.mp3')
            })
        });
    },
    m2play: function(){
        this.preMusicList()
        cc.audioEngine.pauseAllEffects()
        this.s2.node.active = true
        this.currentMusicShow.string = '清溪潺流' 
        cc.loader.load('https://image.taotaoxi.cn/minigame/music/1.mp3', function(err, clip) {
            cc.audioEngine.pause(common.getStorage('audioId'));
            let _audioId = cc.audioEngine.play(clip, true);
            common.setStorage('audioId', _audioId, () => {
                common.setStorage('cm', '1.mp3')
            })
            
        });       
    },
    m3play: function(){
        this.preMusicList()
        cc.audioEngine.pauseAllEffects()
        this.s3.node.active = true
        this.currentMusicShow.string = '心经'    
        cc.loader.load('https://image.taotaoxi.cn/minigame/music/2.mp3', function(err, clip) {
            cc.audioEngine.pause(common.getStorage('audioId'));
            let _audioId = cc.audioEngine.play(clip, true);
            common.setStorage('audioId', _audioId, ()=>{
                common.setStorage('cm', '2.mp3')
            })
        });
    },
    m4play: function(){
        this.preMusicList()
        cc.audioEngine.pauseAllEffects()        
        this.s4.node.active = true
        this.currentMusicShow.string = '消灾吉祥神咒'   
        cc.loader.load('https://image.taotaoxi.cn/minigame/music/3.mp3', function(err, clip) {
            cc.audioEngine.pause(common.getStorage('audioId'));
            let _audioId = cc.audioEngine.play(clip, true);
            common.setStorage('audioId', _audioId, ()=>{
                common.setStorage('cm', '3.mp3')
            })
        });  
    },
    start () {
        if(cc.sys.os === cc.sys.OS_ANDROID||cc.sys.os==cc.sys.OS_IOS){
            cc.view.setDesignResolutionSize(1080, 1920, cc.ResolutionPolicy.FIXED_HEIGHT);
        }else{
            cc.view.setDesignResolutionSize(1080, 1920, cc.ResolutionPolicy.SHOW_ALL);
        }
    },
    update: function(){
        wx.onShow(function () {
            common.audio.play()
        })
    }
});
