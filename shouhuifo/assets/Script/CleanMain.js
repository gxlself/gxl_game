var common = require("Common")
cc.Class({
    extends: cc.Component,

    properties: {
        back: cc.Sprite,
        outMask: cc.Sprite,
        coverMask: cc.Sprite,
        countLabel: cc.Label,
        musicBtn: cc.Sprite
    },
    onLoad: function(){
        this.preloadScene()
        
        this.back.node.on(cc.Node.EventType.TOUCH_START, this.backMain, this)
        this.outMask.node.on(cc.Node.EventType.TOUCH_END, this.outMaskHide, this)
        this.coverMask.node.on(cc.Node.EventType.TOUCH_END, this.beginClean, this)
        this.musicBtn.node.on(cc.Node.EventType.TOUCH_END, this.goMusicScene, this)

        // if(cc.audioEngine.isMusicPlaying()){
        //     this.musicBtn.node.runAction(cc.repeatForever(cc.rotateBy(1.0, 360)))
        // }
        let state = cc.audioEngine.getState(common.getStorage('audioId'))
        if(state == 1){
            this.musicBtn.node.runAction(cc.repeatForever(cc.rotateBy(1.0, 360)))
        }
    
    },
    beginClean: function(){
        // this.node.runAction(cc.sequence(cc.fadeOut(0.1),cc.callFunc(function(){
        //     cc.director.loadScene('ModalCard')
        // })));

        cc.director.loadScene("ModalCard",(err,scene)=>{
            cc.sequence(cc.fadeOut(0.1),cc.callFunc(function(){
                cc.director.loadScene('ModalCard')
            }))
        })
    },
    backMain: function(){
        this.node.runAction(cc.sequence(cc.fadeOut(0.1),cc.callFunc(function(){
            cc.audioEngine.pauseAllEffects()
            cc.director.loadScene('Detail')
        })));
    },
    preloadScene: function(){
        let that = this
        this.countLabel.node.color = new cc.color(22,100,45,255);
        this.countLabel.string = common.countNum()
    
        if(common.isFirstOpen){
            this.outMask.node.active = true
        }else{
            this.outMask.node.active = false
        }
    },
    outMaskHide: function(){
        this.outMask.node.active = false
        common.isFirstOpen = false
    },
    goMusicScene: function(){
        // this.node.runAction(cc.sequence(cc.fadeOut(0.1),cc.callFunc(function(){
        //     cc.director.loadScene('ChooseMusic')
        // })));
        cc.director.loadScene("ChooseMusic",(err,scene)=>{
            cc.sequence(cc.fadeOut(0.1),cc.callFunc(function(){
                cc.director.loadScene('ChooseMusic')
            }))
        })
    },
    start () {
        if(cc.sys.os === cc.sys.OS_ANDROID||cc.sys.os==cc.sys.OS_IOS){
            cc.view.setDesignResolutionSize(1080, 1920, cc.ResolutionPolicy.FIXED_HEIGHT);
        }else{
            cc.view.setDesignResolutionSize(1080, 1920, cc.ResolutionPolicy.SHOW_ALL);
        }
    }
});
