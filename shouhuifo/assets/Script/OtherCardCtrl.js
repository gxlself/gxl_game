var common = require("Common");
cc.Class({
    extends: cc.Component,

    properties: {
        cardBg: cc.Sprite,      // 底层精灵
        //resultLabel: cc.Label,  
        mask: cc.Mask,          // 遮罩组件（实现擦除功能）
        //promptLabel: cc.Label,  // 提示文字
        sucFlagNode: {
            default: [],   
            type: [cc.Node],
            tooltip:"标记需要擦除区域，每个node大小为单次擦除大小，默认30像素。"
        },
        _sucFlag:[cc.Boolean]
    },
    onLoad: function (){
        this.node.on(cc.Node.EventType.TOUCH_START, this._onTouchBegin, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this._onTouchMoved, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this._onTouchEnd, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this._onTouchCancel, this);
        for(var i=0; i<this.sucFlagNode.length; i++){
            this._sucFlag.push(false);
        }
    },
    onDestroy:function () {
        this._removeTouchEvent();
    },
    _removeTouchEvent: function(){
        this.node.off(cc.Node.EventType.TOUCH_START, this._onTouchBegin, this);
        this.node.off(cc.Node.EventType.TOUCH_MOVE, this._onTouchMoved, this);
        this.node.off(cc.Node.EventType.TOUCH_END, this._onTouchEnd, this);
        this.node.off(cc.Node.EventType.TOUCH_CANCEL, this._onTouchCancel, this);
    },
    start:function () {
       // var x =-100;
        // var y =-100;
        // var width =300;
        // var height = 200;
        // var rectangle = [cc.v2(x, y),
        //     cc.v2(x + width, y),
        //     cc.v2(x + width, y + height),
        //     cc.v2(x, y + height)];
        //
        // stencil.drawPoly(rectangle, color, 0, color);

        // stencil.drawPoly(this.mask._calculateCircle(cc.p(0,0),cc.p(100,100), 64), color, 0, color);
        //
        // stencil.drawPoly(this.mask._calculateCircle(cc.p(200,200),cc.p(50,50), 64), color, 0, color);
    },
    _onTouchBegin:function (event) {
        var point = event.touch.getLocation();
        point = this.node.convertToNodeSpaceAR(point);
        this._addCircle(point);
    },
    _onTouchMoved:function (event) {
        var point = event.touch.getLocation();
        point = this.node.convertToNodeSpaceAR(point);
        this._addCircle(point);
    },
    _onTouchEnd:function (event) {
        var point = event.touch.getLocation();
        point = this.node.convertToNodeSpaceAR(point);
        this._addCircle(point);
    },
    _onTouchCancel:function (event) {
        // var point = event.touch.getLocation();
        // point = this.node.convertToNodeSpaceAR(point);
        // this._addCircle(point);
    },
    _addCircle:function (point) {
        var stencil = this.mask._clippingStencil;
        var color = cc.color(255, 255, 255, 0);
        stencil.drawPoly(this.mask._calculateCircle(point,cc.p(50,50), 64), color, 0, color);
        if (!CC_JSB) {
            cc.renderer.childrenOrderDirty = true;
        }    
        this._checkFinish(point);    
    },

    _checkFinish: function(point){
        var res = true;
        for (var i=0; i<this.sucFlagNode.length; i++){
            var node = this.sucFlagNode[i];
            if (cc.rectContainsPoint(node.getBoundingBox(), point)){
                this._sucFlag[i] = true;
            }
            if (!this._sucFlag[i]){
                res = false;
            }
        }
        if (res){
            this._removeTouchEvent();
            cc.log("擦除完成...");
        }
    }
});

