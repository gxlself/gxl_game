// TypeScript file
class zhenMing extends egret.DisplayObject {

    public constructor() {
        super();
        this.start()
    }
    private init() {

        RES.getResByUrl("resource/assets/payBanner.jpg", (res) => {

            var img: egret.Texture = <egret.Texture>res;
            var banner: egret.Bitmap = new egret.Bitmap(img);
            banner.width = GameConfig.getWidth()
            banner.height = GameConfig.getWidth() / 32 * 15
            banner.y = 0
            var banner2: egret.Bitmap = new egret.Bitmap(img);
            banner2.width = GameConfig.getWidth()
            banner2.height = GameConfig.getWidth() / 32 * 15
            banner2.y = GameConfig.getWidth() / 32 * 15
            var bg = new egret.Sprite()
            bg.addChild(banner)
            bg.addChild(banner2)
            var myScroller: egret.ScrollView = new egret.ScrollView();
            //注意位置和尺寸的设置是在Scroller上面，而不是容器上面
            myScroller.width = GameConfig.getWidth();
            myScroller.height = GameConfig.getHeight();
            //设置viewport
            myScroller.bounces = false
            myScroller.setContent(bg);
            // myScroller.setContent(banner2);
            GameConfig.getMain().addChild(myScroller);
        }, this, RES.ResourceItem.TYPE_IMAGE)
        // bird.x = 0
        // bird.y = 0


        // console.log(myScroller)
    }
    private start() {
        //创建一个 Scroller
        var yearScroller: eui.Scroller = new eui.Scroller();
        var monthScroller: eui.Scroller = new eui.Scroller();
        var dayScroller: eui.Scroller = new eui.Scroller();
        //注意位置和尺寸的设置是在Scroller上面，而不是容器上面
        yearScroller.width = GameConfig.getWidth() / 4;
        yearScroller.height = 50;
        yearScroller.x = 0
        yearScroller.y = 0

        monthScroller.width = GameConfig.getWidth() / 4;
        monthScroller.height = 50;
        monthScroller.x = GameConfig.getWidth() / 4
        monthScroller.y = 0

        dayScroller.width = GameConfig.getWidth() / 4;
        dayScroller.height = 50;
        dayScroller.x = GameConfig.getWidth() / 4 * 2
        dayScroller.y = 0
        var yearlist = new eui.List();
        var monthlist = new eui.List();
        var daylist = new eui.List();
        let year = [], month = [], day = []
        for (let i = 1900; i <= new Date().getFullYear(); i++) {
            year.push(i)
        }
        for (let i = 1; i <= 12; i++) {
            month.push(i)
        }
        for (let i = 1; i <= 31; i++) {
            day.push(i)
        }
        yearlist.dataProvider = new eui.ArrayCollection(year);
        yearScroller.viewport = yearlist
        monthlist.dataProvider = new eui.ArrayCollection(month);
        monthScroller.viewport = monthlist
        daylist.dataProvider = new eui.ArrayCollection(day);
        dayScroller.viewport = daylist
        // myScroller.setContent(list)
        yearScroller.addEventListener(eui.UIEvent.CHANGE_END, moveScroller, this);
        monthScroller.addEventListener(eui.UIEvent.CHANGE_END, moveScroller, this);
        dayScroller.addEventListener(eui.UIEvent.CHANGE_END, moveScroller, this);

        yearScroller.bounces = false
        monthScroller.bounces = false
        dayScroller.bounces = false
        let scrollBg = new egret.Sprite()
        scrollBg.y = 400
        scrollBg.addChild(yearScroller)
        scrollBg.addChild(monthScroller)
        scrollBg.addChild(dayScroller)
        GameConfig.getMain().addChild(scrollBg);
        let birth = ""
        function moveScroller() {
            if ((yearScroller.viewport.scrollV + yearScroller.height) >= yearScroller.viewport.contentHeight || yearScroller.viewport.scrollV < 0) {
                console.log("滚动到底部了");
                birth += year[year.length - 1]
            } else {
                let num = Math.floor(yearScroller.viewport.scrollV / 50)
                if (yearScroller.viewport.scrollV % 50 > 30) {
                    num++
                }
                birth += year[num]
                yearScroller.viewport.scrollV = num * 50
            }
            if ((monthScroller.viewport.scrollV + monthScroller.height) >= monthScroller.viewport.contentHeight || monthScroller.viewport.scrollV < 0) {
                console.log("滚动到底部了");
                birth += month[month.length - 1]
            } else {
                let num = Math.floor(monthScroller.viewport.scrollV / 50)
                if (monthScroller.viewport.scrollV % 50 > 30) {
                    num++
                }
                birth += month[num]
                monthScroller.viewport.scrollV = num * 50
            }
            if ((dayScroller.viewport.scrollV + dayScroller.height) >= dayScroller.viewport.contentHeight || dayScroller.viewport.scrollV < 0) {
                console.log("滚动到底部了");
                birth += day[day.length - 1]
            } else {
                let num = Math.floor(dayScroller.viewport.scrollV / 50)
                if (dayScroller.viewport.scrollV % 50 > 30) {
                    num++
                }
                birth += day[num]
                dayScroller.viewport.scrollV = num * 50
            }
            console.log(birth)
            birth = ""
        }

        let bgm_btn = eKit.createSprite({ x: 20, y: 220 });
        GameConfig.getMain().addChild(bgm_btn);
        let bgm_btn_bg = eKit.createRect([0, 0, 120, 60], { beginFill: { color: 0x9988ff, alpha: 1 } }, { touchEnabled: true });
        bgm_btn.addChild(bgm_btn_bg);
        bgm_btn.addChild(eKit.createText('切换音乐', { width: 120, height: 60, size: 20, textAlign: 'center', verticalAlign: 'middle' }));
        bgm_btn_bg.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            // Mp3.switchBgm('bgm_game');
            moveScroller()
        }, this);
    }


}