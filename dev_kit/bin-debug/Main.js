//////////////////////////////////////////////////////////////////////////////////////
//
//  Copyright (c) 2014-present, Egret Technology.
//  All rights reserved.
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the Egret nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY EGRET AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
//  OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL EGRET AND CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, DATA,
//  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//////////////////////////////////////////////////////////////////////////////////////
var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var Main = (function (_super) {
    __extends(Main, _super);
    function Main() {
        var _this = _super.call(this) || this;
        _this.addEventListener(egret.Event.ADDED_TO_STAGE, _this.createChildren, _this);
        return _this;
    }
    Main.prototype.createChildren = function () {
        egret.lifecycle.addLifecycleListener(function (context) {
            // custom lifecycle plugin
        });
        egret.lifecycle.onPause = function () {
            egret.ticker.pause();
        };
        egret.lifecycle.onResume = function () {
            egret.ticker.resume();
        };
        this.runGame().catch(function (e) {
            console.log(e);
        });
    };
    Main.prototype.runGame = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadResource()];
                    case 1:
                        _a.sent();
                        this.createGameScene();
                        return [2 /*return*/];
                }
            });
        });
    };
    Main.prototype.loadResource = function () {
        return __awaiter(this, void 0, void 0, function () {
            var loadingView, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, , 5]);
                        loadingView = new LoadingUI();
                        loadingView.visible = false;
                        this.stage.addChild(loadingView);
                        return [4 /*yield*/, RES.loadConfig("resource/default.res.json", "resource/")];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, new eui.Theme("resource/default.thm.json", this.stage)];
                    case 2:
                        _a.sent();
                        console.log('load_1');
                        return [4 /*yield*/, RES.loadGroup("preload", 0, null)];
                    case 3:
                        _a.sent();
                        // await RES.loadGroup("banner", 0, loadingView);
                        console.log('load_2');
                        this.stage.removeChild(loadingView);
                        return [3 /*break*/, 5];
                    case 4:
                        e_1 = _a.sent();
                        console.error(e_1);
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * 创建场景界面
     * Create scene interface
     */
    Main.prototype.createGameScene = function () {
        var _this = this;
        // 设置游戏参数内的默认舞台宽高
        GameConfig.setStageWidthHeight(this.stage);
        // 刷新版本控制数据
        VersionCtrl.refreshVersionCtrl();
        GameConfig.setMain(this.stage);
        // setInterval(moveScroller,50)
        // var listGroup = new skins.dateList();
        // listGroup.width = GameConfig.getWidth();
        // listGroup.height = 500;
        // listGroup.x = 0;
        // listGroup.y = 200;
        // listGroup.touchChildren = true
        // listGroup.touchEnabled = true
        // listGroup.viewport.scrollV
        // listGroup["list1"].addEventListener(egret.TouchEvent.TOUCH_TAP, (res) => {
        //     console.log(res)
        // }, this)
        // GameConfig.getMain().addChild(listGroup);
        //  console.log(res.itemIndex) eui.ItemTapEvent.ITEM_TAP
        // 绘制背景
        // let sky = eKit.createBitmapByName("bg_jpg", { width: GameConfig.getWidth(), height: GameConfig.getHeight(), x: 0, y: 0, touchEnabled: true });
        // this.addChild(sky);
        /**
         *常用调用示例,不需要登录的在第一列
         *
         */
        // 切换背景音乐调用示例
        // // 事件音乐调用示例
        // let music_btn = eKit.createSprite({ x: 20, y: 100 });
        // this.addChild(music_btn);
        // let music_btn_bg = eKit.createRect([0, 0, 120, 60], { beginFill: { color: 0x9988ff, alpha: 1 } }, { touchEnabled: true });
        // music_btn.addChild(music_btn_bg);
        // music_btn.addChild(eKit.createText('调用事件音乐', { width: 120, height: 60, size: 20, textAlign: 'center', verticalAlign: 'middle' }));
        // music_btn_bg.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
        //     Mp3.stopBGM()
        //     Mp3.playEvent('hit');
        // }, this);
        // 触发粒子特效
        var particle_btn = eKit.createSprite({ x: 20, y: 180 });
        this.addChild(particle_btn);
        var particle_btn_bg = eKit.createRect([0, 0, 120, 60], { beginFill: { color: 0x9988ff, alpha: 1 } }, { touchEnabled: true });
        particle_btn.addChild(particle_btn_bg);
        particle_btn.addChild(eKit.createText('触发粒子', { width: 120, height: 60, size: 20, textAlign: 'center', verticalAlign: 'middle' }));
        particle_btn_bg.addEventListener(egret.TouchEvent.TOUCH_TAP, function () { return __awaiter(_this, void 0, void 0, function () {
            var system;
            return __generator(this, function (_a) {
                system = new particle.GravityParticleSystem(RES.getRes('boom_png'), RES.getRes('boom_json'));
                system.start(50);
                this.addChild(system);
                return [2 /*return*/];
            });
        }); }, this);
        // // 清空按钮
        // let clear_btn = eKit.createSprite({ x: 20, y: 260 });
        // this.addChild(clear_btn);
        // let clear_btn_bg = eKit.createRect([0, 0, 120, 60], { beginFill: { color: 0x9988ff, alpha: 1 } }, { touchEnabled: true });
        // clear_btn.addChild(clear_btn_bg);
        // clear_btn.addChild(eKit.createText('清空按钮', { width: 120, height: 60, size: 20, textAlign: 'center', verticalAlign: 'middle' }));
        // clear_btn_bg.addEventListener(egret.TouchEvent.TOUCH_TAP, async () => {
        //     eKit.clearView(this, 0);
        // }, this);
        // // 显示banner广告,请先确保你的小游戏拥有流量主权限
        // let banner_ad = null;
        // let show_banner_ad_btn = eKit.createSprite({ x: 20, y: 340 });
        // this.addChild(show_banner_ad_btn);
        // let show_banner_ad_btn_bg = eKit.createRect([0, 0, 120, 60], { beginFill: { color: 0x9988ff, alpha: 1 } }, { touchEnabled: true });
        // show_banner_ad_btn.addChild(show_banner_ad_btn_bg);
        // show_banner_ad_btn.addChild(eKit.createText('显示BANNER广告', { width: 120, height: 60, size: 20, textAlign: 'center', verticalAlign: 'middle' }));
        // show_banner_ad_btn_bg.addEventListener(egret.TouchEvent.TOUCH_TAP, async () => {
        //     // showBannerAd第一个参数填入广告id
        //     !banner_ad && (banner_ad = WxKit.showBannerAd(''));
        // }, this);
        // // 隐藏banner广告
        // let hide_banner_ad_btn = eKit.createSprite({ x: 20, y: 420 });
        // this.addChild(hide_banner_ad_btn);
        // let hide_banner_ad_btn_bg = eKit.createRect([0, 0, 120, 60], { beginFill: { color: 0x9988ff, alpha: 1 } }, { touchEnabled: true });
        // hide_banner_ad_btn.addChild(hide_banner_ad_btn_bg);
        // hide_banner_ad_btn.addChild(eKit.createText('隐藏BANNER广告', { width: 120, height: 60, size: 20, textAlign: 'center', verticalAlign: 'middle' }));
        // hide_banner_ad_btn_bg.addEventListener(egret.TouchEvent.TOUCH_TAP, async () => {
        //     banner_ad && typeof banner_ad.hide == 'function' && (banner_ad.hide(), banner_ad.destory(), banner_ad = null);
        // }, this);
        // // 显示视频广告
        // let show_video_ad_btn = eKit.createSprite({ x: 20, y: 500 });
        // this.addChild(show_video_ad_btn);
        // let show_video_ad_btn_bg = eKit.createRect([0, 0, 120, 60], { beginFill: { color: 0x9988ff, alpha: 1 } }, { touchEnabled: true });
        // show_video_ad_btn.addChild(show_video_ad_btn_bg);
        // show_video_ad_btn.addChild(eKit.createText('显示视频广告', { width: 120, height: 60, size: 20, textAlign: 'center', verticalAlign: 'middle' }));
        // show_video_ad_btn_bg.addEventListener(egret.TouchEvent.TOUCH_TAP, async () => {
        //     // showVideoAd第一个参数填入广告id
        //     WxKit.showVideoAd('',
        //         () => {
        //             //成功回调
        //             wx.showToast({ title: '观看广告成功', icon: 'success', image: null })
        //         },
        //         () => {
        //             //失败回调
        //             wx.showToast({ title: '观看广告失败', icon: null, image: null })
        //         });
        // }, this);
        /**
         * 常用调用示例,需要登录的在第二列
         *
         */
        // 登录调用示例
        var login_btn = eKit.createSprite({ x: 160, y: 20 });
        this.addChild(login_btn);
        var login_btn_bg = eKit.createRect([0, 0, 120, 60], { beginFill: { color: 0x9988ff, alpha: 1 } }, { touchEnabled: true });
        login_btn.addChild(login_btn_bg);
        login_btn.addChild(eKit.createText('调用登录', { width: 120, height: 60, size: 20, textAlign: 'center', verticalAlign: 'middle' }));
        login_btn.addEventListener(egret.TouchEvent.TOUCH_TAP, function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: 
                    // 调用WxKit.login完成微信登陆授权操作，返回openId,token等数据;
                    return [4 /*yield*/, WxKit.login()];
                    case 1:
                        // 调用WxKit.login完成微信登陆授权操作，返回openId,token等数据;
                        _a.sent();
                        // console.log(UserData.getOpenId());
                        wx.hideLoading();
                        // 设置默认分享,需要登录后方可调用分享功能
                        WxKit.setDefaultShare();
                        WxKit.setOnShowRule();
                        return [2 /*return*/];
                }
            });
        }); }, this);
        // 分享调用示例
        var share_btn = eKit.createSprite({ x: 160, y: 100 });
        this.addChild(share_btn);
        var btn_bg = eKit.createRect([0, 0, 120, 60], { beginFill: { color: 0x9988ff, alpha: 1 } }, { touchEnabled: true });
        share_btn.addChild(btn_bg);
        share_btn.addChild(eKit.createText('调用分享', { width: 120, height: 60, size: 20, textAlign: 'center', verticalAlign: 'middle' }));
        share_btn.addEventListener(egret.TouchEvent.TOUCH_TAP, function () { WxKit.shareGame('normalShare', '我就试试分享', ''); }, this);
        // // 调用开放数据域获取好友排行榜
        // let friend_ranking_btn = eKit.createSprite({ x: 160, y: 180 });
        // this.addChild(friend_ranking_btn);
        // let friend_ranking_btn_bg = eKit.createRect([0, 0, 120, 60], { beginFill: { color: 0x9988ff, alpha: 1 } }, { touchEnabled: true });
        // friend_ranking_btn.addChild(friend_ranking_btn_bg);
        // friend_ranking_btn.addChild(eKit.createText('好友排行榜', { width: 120, height: 60, size: 20, textAlign: 'center', verticalAlign: 'middle' }));
        // friend_ranking_btn_bg.addEventListener(egret.TouchEvent.TOUCH_TAP, async () => {
        //     let open_data: egret.Sprite = WxKit.linkOpenData({});
        //     this.addChild(open_data);
        // }, this);
        // Mp3.loadEventSound();
        // 播放背景音乐
        // wx.onShow(()=>{
        // console.log(wx.createInnerAudioContext())
        // Mp3.switchBgm("hit")
        // Mp3.playBGM()
        // })
        //         setTimeout(function(){
        // var bgm = wx.createInnerAudioContext()
        // bgm.autoplay = true
        // bgm.loop = true
        // bgm.src = 'resource/assets/mp3/bgm.mp3'
        // bgm.play()
        //         },100)
        // Mp3.switchBgm('bgm_default');
        // var banner: egret.ImageLoader = new egret.ImageLoader;
        // banner.once(egret.Event.COMPLETE, this.imgLoadHandler, this);
        // banner.load();
        new zhenMing();
    };
    return Main;
}(egret.DisplayObjectContainer));
__reflect(Main.prototype, "Main");
