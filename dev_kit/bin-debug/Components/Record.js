var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var Records = (function () {
    function Records() {
    }
    /**
     * 获取个人周最高成绩
     */
    Records.getWeekScore = function () {
        return __awaiter(this, void 0, void 0, function () {
            var weekTime, postData, record;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        weekTime = Utils.getWeekTime();
                        postData = {
                            record_type: 1
                        };
                        record = 0;
                        return [4 /*yield*/, Api.getBestRecord(1).then(function (res) { record = res['week_best']; })];
                    case 1:
                        _a.sent();
                        this.weekScore = record;
                        return [2 /*return*/, record];
                }
            });
        });
    };
    /**
     * 获取个人历史最高成绩
     */
    Records.getHistoryScore = function () {
        return __awaiter(this, void 0, void 0, function () {
            var postData, record;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        postData = {
                            record_type: 1
                        };
                        record = 0;
                        return [4 /*yield*/, Api.getBestRecord(1).then(function (res) { record = res['history_best']; })];
                    case 1:
                        _a.sent();
                        this.historyScore = record;
                        return [2 /*return*/, record];
                }
            });
        });
    };
    /**
     * 网络刷新世界排行榜数据
     */
    Records.refreshWorldRanking = function () {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        result = null;
                        return [4 /*yield*/, Api.getRankings().then(function (res) { result = res; })];
                    case 1:
                        _a.sent();
                        this.worldRankings = result;
                        return [2 /*return*/, result];
                }
            });
        });
    };
    /**
     * 获取世界排行榜数据
     */
    Records.getRankings = function () {
        return JSON.parse(JSON.stringify(this.worldRankings));
    };
    /**
     * 获取当前成绩
     */
    Records.getScore = function () {
        return this.score;
    };
    /**
     * 更新当前成绩
     */
    Records.updateScore = function (score) {
        this.score = score || 0;
    };
    /**
     * 更新当前游戏类型
     */
    Records.updateRecordType = function (record_type) {
        this.record_type = record_type || 0;
    };
    /**
     * 上传成绩，同时上传至腾讯云及自己服务器
     */
    Records.uploadScore = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var uploadData;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        uploadData = { score: (this.score || 0), record_type: (this.record_type || 1) };
                        // 若网络不好，上传自己服务器失败，则重新上传（后期需完善若超时规则）
                        return [4 /*yield*/, Api.uploadRecords(uploadData)
                                .then(function (result) { return __awaiter(_this, void 0, void 0, function () { var _a; return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = !result;
                                        if (!_a) return [3 /*break*/, 2];
                                        return [4 /*yield*/, Records.updateScore()];
                                    case 1:
                                        _a = (_b.sent());
                                        _b.label = 2;
                                    case 2:
                                        _a;
                                        return [2 /*return*/];
                                }
                            }); }); })];
                    case 1:
                        // 若网络不好，上传自己服务器失败，则重新上传（后期需完善若超时规则）
                        _a.sent();
                        return [4 /*yield*/, WxKit.uploadScore(this.score || 0)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/, true];
                }
            });
        });
    };
    // 玩家当前局成绩
    Records.score = 62;
    return Records;
}());
__reflect(Records.prototype, "Records");
