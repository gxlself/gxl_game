var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
/**
   * eKit为优化使用方法后的egret引擎常用函数调用，包括创建bitmap，text，shape等
   *
   */
var eKit = (function () {
    function eKit() {
    }
    /**
     * 根据name关键字创建一个Bitmap对象。name属性请参考resources/resource.json配置文件的内容。
     * @param  {string} name
     * @param  {Object} settings?
     */
    eKit.createSprite = function (settings) {
        var result = new egret.Sprite();
        if (settings) {
            for (var key in settings) {
                result[key] = settings[key];
            }
        }
        return result;
    };
    /**
     * 根据name关键字创建一个Bitmap对象。name属性请参考resources/resource.json配置文件的内容。
     * @param  {string} name
     * @param  {Object} settings?
     */
    eKit.createBitmapByName = function (name, settings) {
        var result = new egret.Bitmap();
        var texture = RES.getRes(name);
        result.texture = texture;
        if (settings) {
            for (var key in settings) {
                result[key] = settings[key];
            }
        }
        return result;
    };
    /**
     * 根据text创建TextField对象
     * @param  {string} text
     * @param  {Object} settings?
     */
    eKit.createText = function (text, settings) {
        var result = new egret.TextField();
        result.text = text;
        if (settings) {
            for (var key in settings) {
                result[key] = settings[key];
            }
        }
        return result;
    };
    /**
     * 异步根据URL获取头像
     * @param  {any=''} url
     * @param  {Object} settings?
     */
    eKit.createAvatar = function (url, settings) {
        if (url === void 0) { url = ''; }
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        if (!url) {
                            reject('无头像');
                        }
                        RES.getResByUrl(url, function (evt) {
                            var textTure = evt;
                            var bitmap = new egret.Bitmap(textTure);
                            if (settings) {
                                for (var key in settings) {
                                    bitmap[key] = settings[key];
                                }
                            }
                            resolve(bitmap);
                        }, _this, RES.ResourceItem.TYPE_IMAGE);
                    })];
            });
        });
    };
    /**
     * 根据参数绘制直线段
     * @param  {Array<any>} points
     * @param  {{beginFill?:{color:number} param
     * @param  {number}} alpha
     * @param  {{thickness?:number} lineStyle?
     * @param  {number} color?
     * @param  {number} alpha?
     * @param  {boolean} pixelHinting?
     * @param  {string} scaleMode?
     * @param  {string} caps?
     * @param  {string} joints?
     * @param  {number}}} miterLimit?
     * @param  {Object} settings?
     * @returns egret
     */
    eKit.createLine = function (points, param, settings) {
        var shp = new egret.Shape();
        if (param.beginFill) {
            shp.graphics.beginFill(param.beginFill.color, param.beginFill.alpha);
        }
        if (param.lineStyle) {
            shp.graphics.lineStyle(param.lineStyle.thickness, param.lineStyle.color, param.lineStyle.alpha, param.lineStyle.pixelHinting, param.lineStyle.scaleMode, param.lineStyle.caps, param.lineStyle.joints, param
                .lineStyle.miterLimit);
        }
        shp.graphics.moveTo(points[0][0], points[0][1]);
        points.map(function (point, index) {
            index > 0 && shp.graphics.lineTo(points[index][0], points[index][1]);
        });
        shp.graphics.endFill();
        if (settings) {
            for (var key in settings) {
                shp[key] = settings[key];
            }
        }
        return shp;
    };
    /**
     * 根据参数绘制矩形
     * @param  {Array<any>} points
     * @param  {{beginFill?:{color:number} param
     * @param  {number}} alpha
     * @param  {{thickness?:number} lineStyle?
     * @param  {number} color?
     * @param  {number} alpha?
     * @param  {boolean} pixelHinting?
     * @param  {string} scaleMode?
     * @param  {string} caps?
     * @param  {string} joints?
     * @param  {number}}} miterLimit?
     * @param  {Object} settings?
     * @returns egret
     */
    eKit.createRect = function (points, param, settings) {
        var shp = new egret.Shape();
        if (param.beginFill) {
            shp.graphics.beginFill(param.beginFill.color, param.beginFill.alpha);
        }
        if (param.lineStyle) {
            shp.graphics.lineStyle(param.lineStyle.thickness, param.lineStyle.color, param.lineStyle.alpha, param.lineStyle.pixelHinting, param.lineStyle.scaleMode, param.lineStyle.caps, param.lineStyle.joints, param
                .lineStyle.miterLimit);
        }
        shp.graphics.drawRect(points[0], points[1], points[2], points[3]);
        if (settings) {
            for (var key in settings) {
                shp[key] = settings[key];
            }
        }
        return shp;
    };
    /**
     * 根据参数绘制圆形
     * @param  {Array<any>} points
     * @param  {{beginFill?:{color:number} param
     * @param  {number}} alpha
     * @param  {{thickness?:number} lineStyle?
     * @param  {number} color?
     * @param  {number} alpha?
     * @param  {boolean} pixelHinting?
     * @param  {string} scaleMode?
     * @param  {string} caps?
     * @param  {string} joints?
     * @param  {number}}} miterLimit?
     * @param  {Object} settings?
     * @returns egret
     */
    eKit.createCircle = function (points, param, settings) {
        var shp = new egret.Shape();
        if (param.beginFill) {
            shp.graphics.beginFill(param.beginFill.color, param.beginFill.alpha);
        }
        if (param.lineStyle) {
            shp.graphics.lineStyle(param.lineStyle.thickness, param.lineStyle.color, param.lineStyle.alpha, param.lineStyle.pixelHinting, param.lineStyle.scaleMode, param.lineStyle.caps, param.lineStyle.joints, param
                .lineStyle.miterLimit);
        }
        shp.graphics.drawCircle(points[0], points[1], points[2]);
        if (settings) {
            for (var key in settings) {
                shp[key] = settings[key];
            }
        }
        return shp;
    };
    /**
     * 根据参数绘制圆弧路径
     * @param  {Array<any>} points
     * @param  {{beginFill?:{color:number} param
     * @param  {number}} alpha
     * @param  {{thickness?:number} lineStyle?
     * @param  {number} color?
     * @param  {number} alpha?
     * @param  {boolean} pixelHinting?
     * @param  {string} scaleMode?
     * @param  {string} caps?
     * @param  {string} joints?
     * @param  {number}}} miterLimit?
     * @param  {Object} settings?
     * @returns egret
     */
    eKit.createArc = function (points, param, settings) {
        var shp = new egret.Shape();
        if (param.beginFill) {
            shp.graphics.beginFill(param.beginFill.color, param.beginFill.alpha);
        }
        if (param.lineStyle) {
            shp.graphics.lineStyle(param.lineStyle.thickness, param.lineStyle.color, param.lineStyle.alpha, param.lineStyle.pixelHinting, param.lineStyle.scaleMode, param.lineStyle.caps, param.lineStyle.joints, param
                .lineStyle.miterLimit);
        }
        shp.graphics.drawArc(points[0], points[1], points[2], points[3], points[4], points[5]);
        if (settings) {
            for (var key in settings) {
                shp[key] = settings[key];
            }
        }
        return shp;
    };
    /**
     * 根据参数绘制圆角矩形
     * @param  {Array<any>} points
     * @param  {{beginFill?:{color:number} param
     * @param  {number}} alpha
     * @param  {{thickness?:number} lineStyle?
     * @param  {number} color?
     * @param  {number} alpha?
     * @param  {boolean} pixelHinting?
     * @param  {string} scaleMode?
     * @param  {string} caps?
     * @param  {string} joints?
     * @param  {number}}} miterLimit?
     * @param  {Object} settings?
     * @returns egret
     */
    eKit.createRoundRect = function (points, param, settings) {
        var shp = new egret.Shape();
        if (param.beginFill) {
            shp.graphics.beginFill(param.beginFill.color, param.beginFill.alpha);
        }
        if (param.lineStyle) {
            shp.graphics.lineStyle(param.lineStyle.thickness, param.lineStyle.color, param.lineStyle.alpha, param.lineStyle.pixelHinting, param.lineStyle.scaleMode, param.lineStyle.caps, param.lineStyle.joints, param
                .lineStyle.miterLimit);
        }
        shp.graphics.drawRoundRect(points[0], points[1], points[2], points[3], points[4], points[5]);
        if (settings) {
            for (var key in settings) {
                shp[key] = settings[key];
            }
        }
        return shp;
    };
    /**
     * 传入一个DisplayObject，将其从其父元素上移除
     * @param  {egret.DisplayObject} children
     */
    eKit.removeChild = function (children) {
        if (children.parent) {
            children.parent.removeChild(children);
        }
    };
    /**
     * 清空显示容器内显示元素，可输入start防止索引号之前的元素被清空
     * @param  {egret.DisplayObjectContainer} displayObjectContainer
     * @param  {number} start?
     */
    eKit.clearView = function (displayObjectContainer, start) {
        isNaN(start) && (start = -1);
        while (displayObjectContainer.$children.length > start + 1) {
            displayObjectContainer.removeChildAt(start + 1);
        }
        return true;
    };
    return eKit;
}());
__reflect(eKit.prototype, "eKit");
