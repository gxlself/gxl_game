/**
   * WxKit为优化使用小游戏中常用的函数方法调用
   *
   */
var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var PLATFORM = new WxPlatform();
var WxKit = (function () {
    function WxKit() {
    }
    /**
     * 调用login完成getUserInfo版登陆操作
     */
    WxKit.login = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var code, userInfo, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        code = null;
                        userInfo = null;
                        result = null;
                        return [4 /*yield*/, PLATFORM.login()
                                .then(function (res) { code = res.code; })
                                .catch(function (err) { console.warn(err); })];
                    case 1:
                        _a.sent();
                        // 调用 wx.getUserInfo
                        return [4 /*yield*/, PLATFORM.getUserInfo()
                                .then(function (res) { return __awaiter(_this, void 0, void 0, function () {
                                var userInfo;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            userInfo = JSON.parse(JSON.stringify(res));
                                            WxKit.iv = userInfo.iv;
                                            WxKit.enctypecode = userInfo.encryptedData;
                                            // 调用自己的服务器登录接口
                                            return [4 /*yield*/, PLATFORM.auth(code, userInfo.iv, userInfo.encryptedData)
                                                    .then(function (res) {
                                                    result = JSON.parse(JSON.stringify(res));
                                                    // 设置通讯token
                                                    Api.setToken(result.data.token);
                                                    // 存入用户数据
                                                    UserData.setUserData(result.data);
                                                    console.log('login_success');
                                                })
                                                    .catch(function (err) { console.warn(err); })];
                                        case 1:
                                            // 调用自己的服务器登录接口
                                            _a.sent();
                                            console.warn('get_user_info_success');
                                            console.log(userInfo);
                                            return [2 /*return*/];
                                    }
                                });
                            }); })
                                .catch(function (err) { return __awaiter(_this, void 0, void 0, function () {
                                var _this = this;
                                var button;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            if (!(typeof wx.createUserInfoButton != 'function')) return [3 /*break*/, 2];
                                            // 重授权弹窗调用，若拒绝会再次弹出
                                            return [4 /*yield*/, WxKit.reAuth()
                                                    .then(function (res) { return __awaiter(_this, void 0, void 0, function () {
                                                    return __generator(this, function (_a) {
                                                        switch (_a.label) {
                                                            case 0:
                                                                userInfo = JSON.parse(JSON.stringify(res));
                                                                // 授权成功后登录
                                                                return [4 /*yield*/, PLATFORM.auth(code, userInfo.iv, userInfo.encryptedData)
                                                                        .then(function (res) {
                                                                        result = JSON.parse(JSON.stringify(res));
                                                                        Api.setToken(result.data.token);
                                                                        UserData.setUserData(result.data);
                                                                        console.log('login_success');
                                                                    })
                                                                        .catch(function (err) {
                                                                        console.warn(err);
                                                                    })];
                                                            case 1:
                                                                // 授权成功后登录
                                                                _a.sent();
                                                                return [2 /*return*/];
                                                        }
                                                    });
                                                }); })];
                                        case 1:
                                            // 重授权弹窗调用，若拒绝会再次弹出
                                            _a.sent();
                                            return [3 /*break*/, 3];
                                        case 2:
                                            // else 方法进入新版调用 createUserInfoButton授权弹窗
                                            wx.hideLoading();
                                            button = wx.createUserInfoButton(WxLoginButton.btnSkin);
                                            button.show();
                                            button.onTap(function (res) { return __awaiter(_this, void 0, void 0, function () {
                                                return __generator(this, function (_a) {
                                                    switch (_a.label) {
                                                        case 0:
                                                            console.log(res);
                                                            if (!(res.errMsg == 'getUserInfo:ok')) return [3 /*break*/, 2];
                                                            WxKit.iv = res.iv;
                                                            WxKit.enctypecode = res.encryptedData;
                                                            return [4 /*yield*/, PLATFORM.auth(code, WxKit.iv, WxKit.enctypecode)
                                                                    .then(function (res) {
                                                                    result = JSON.parse(JSON.stringify(res));
                                                                    Api.setToken(result.data.token);
                                                                    UserData.setUserData(result.data);
                                                                    console.log('login_success');
                                                                })
                                                                    .catch(function (err) { console.warn(err); })];
                                                        case 1:
                                                            _a.sent();
                                                            button.hide();
                                                            return [3 /*break*/, 3];
                                                        case 2: return [2 /*return*/, false];
                                                        case 3: return [2 /*return*/];
                                                    }
                                                });
                                            }); });
                                            _a.label = 3;
                                        case 3: return [2 /*return*/];
                                    }
                                });
                            }); })];
                    case 2:
                        // 调用 wx.getUserInfo
                        _a.sent();
                        return [2 /*return*/, result];
                }
            });
        });
    };
    /**
     * getUserInfo授权失败时重新弹出需授权弹窗,若拒绝则继续弹出
     */
    WxKit.reAuth = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                wx.hideLoading();
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        PLATFORM.showAuthModal()
                            .then(function (res) { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        if (!res.authSetting['scope.userInfo']) return [3 /*break*/, 2];
                                        return [4 /*yield*/, PLATFORM.getUserInfo().then(function (res) { resolve(res); })];
                                    case 1:
                                        _a.sent();
                                        return [3 /*break*/, 4];
                                    case 2: return [4 /*yield*/, WxKit.reAuth().then(function (res) { resolve(res); })];
                                    case 3:
                                        _a.sent();
                                        _a.label = 4;
                                    case 4: return [2 /*return*/];
                                }
                            });
                        }); });
                    })];
            });
        });
    };
    /**
     * 设置默认分享
     */
    WxKit.setDefaultShare = function () {
        console.log('set_default_share');
        wx.showShareMenu({
            withShareTicket: true,
            success: function (res) { console.log('setting_success'); console.log(res); },
            fail: function (err) { console.warn(err); }
        });
        wx.onShareAppMessage(function () {
            return {
                title: GameConfig.getShareTitle() || '',
                imageUrl: GameConfig.getShareImg() || ''
            };
        });
    };
    /**
     * @param  {string} type? type可能取值 ： groupRank(排行榜群排行) groupResult(结果页群排行) reborn(分享重生) normalShare(普通分享)
     * @param  {string} title?
     * @param  {string} imageUrl?
     */
    WxKit.shareGame = function (type, title, imageUrl) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        // 不传type时，默认为普通分享
                        type || (type = 'normalShare');
                        // 不传title时，为默认title
                        title || (title = GameConfig.getShareTitle());
                        // 不传imageUrl时，为默认image
                        imageUrl || (imageUrl = GameConfig.getShareImg());
                        if (!(imageUrl == 'get')) return [3 /*break*/, 2];
                        return [4 /*yield*/, Api.getShareUrl()];
                    case 1:
                        imageUrl = _a.sent();
                        _a.label = 2;
                    case 2: return [2 /*return*/, new Promise(function (resolve, reject) {
                            wx.shareAppMessage({
                                title: title,
                                imageUrl: imageUrl,
                                query: type.match('group') ? 'groupRank=1' : '',
                                success: function (res) {
                                    resolve(res);
                                },
                                fail: function (err) { resolve(null); }
                            });
                        })];
                }
            });
        });
    };
    /**
     * 分享重生调用
     * @param  {string} title
     * @param  {string} imgUrl?
     */
    WxKit.rebornGame = function (title, imgUrl) {
        return __awaiter(this, void 0, void 0, function () {
            var reborn_result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        reborn_result = false;
                        return [4 /*yield*/, WxKit.shareGame('reborn', title, imgUrl).then(function (res) { reborn_result = !!res; })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, reborn_result];
                }
            });
        });
    };
    WxKit.linkOpenData = function (message, width, height) {
        var basic = {
            isDisplay: "true",
            token: Api.getToken(),
            userInfo: UserData.getUserData()
        };
        for (var key in message) {
            basic[key] = message[key];
        }
        var open_data_container = new egret.Sprite();
        var openDataContext = wx.getOpenDataContext();
        var bitmapdata = new egret.BitmapData(window["sharedCanvas"]);
        bitmapdata.$deleteSource = false;
        var texture = new egret.Texture();
        texture._setBitmapData(bitmapdata);
        var bitmap;
        bitmap = new egret.Bitmap(texture);
        bitmap.width = width || GameConfig.getWidth();
        bitmap.height = height || GameConfig.getHeight();
        bitmap.name = "openData";
        open_data_container.addChild(bitmap);
        egret.startTick(function (timeStarmp) {
            egret.WebGLUtils.deleteWebGLTexture(bitmapdata.webGLTexture);
            bitmapdata.webGLTexture = null;
            return false;
        }, this);
        openDataContext.postMessage(basic);
        console.log('link_done');
        return open_data_container;
    };
    // 上传成绩至开放数据域
    WxKit.uploadScore = function (score) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, PLATFORM.setKVData({ "score": score + '', "date": Utils.getNowDate() })
                            .then(function (res) { })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, true];
                }
            });
        });
    };
    /**
     * 设置回到前台事件处理音频及群排行
     */
    WxKit.setOnShowRule = function () {
        wx.onShow(function () {
            Mp3.playBGM();
        });
    };
    WxKit.showVideoAd = function (ad_id, success_callback, err_callback) {
        var _this = this;
        // 无ad_id时弹出警告
        if (!(ad_id)) {
            wx.showModal({
                title: '系统提示', content: "请填入ad_id", showCancel: false, cancelText: "", confirmText: "确定",
                success: function () {
                    err_callback();
                }
            });
            return;
        }
        // 低版本兼容方法
        if (!(typeof wx.createRewardedVideoAd == 'function')) {
            wx.showModal({
                title: '系统提示', content: "您的微信版本太低，暂时无法获取广告", showCancel: false, cancelText: "", confirmText: "确定",
                success: function () {
                    success_callback();
                }
            });
            return;
        }
        this.video_ads[ad_id] = wx.createRewardedVideoAd({
            adUnitId: ad_id
        });
        this.current_video_ad_id = ad_id;
        // 播放广告时暂停背景音乐
        Mp3.stopBGM();
        this.video_ads[ad_id].load()
            .then(function () {
            // 加载成功后播放视频
            _this.video_ads[ad_id].show();
        })
            .catch(function (err) {
            wx.showModal({
                title: '系统提示', content: "暂时无法获取广告", showCancel: false, cancelText: "", confirmText: "确定",
                success: function () {
                    _this.current_video_ad_id == ad_id && success_callback() && (_this.current_video_ad_id = '');
                }
            });
        });
        // 兼容新老版本广告关闭按钮
        this.video_ads[ad_id].onClose(function onCloseFunc(status) {
            if (!status || status.isEnded) {
                // 用户完整观看广告
                WxKit.current_video_ad_id == ad_id && success_callback() && (WxKit.current_video_ad_id = '');
            }
            else {
                // 用户提前点击了【关闭广告】按钮,进入失败回调
                err_callback && WxKit.current_video_ad_id == ad_id && err_callback() && (WxKit.current_video_ad_id = '');
            }
            // 关闭后重开背景音乐
            Mp3.playBGM();
            // 停止监听close方法
            WxKit.video_ads[ad_id].offClose(onCloseFunc);
        });
    };
    /**
     * banner广告调用方法
     */
    WxKit.showBannerAd = function (ad_id) {
        // 无ad_id时弹出警告
        if (!(ad_id)) {
            wx.showModal({
                title: '系统提示', content: "请填入ad_id", showCancel: false, cancelText: "", confirmText: "确定",
                success: function () { }
            });
            return null;
        }
        // 低版本兼容方法
        var bannerAd = typeof wx.createBannerAd == 'function' ? wx.createBannerAd({
            adUnitId: ad_id,
            style: {
                left: 0,
                top: 0,
                width: 350
            }
        }) : null;
        if (bannerAd) {
            bannerAd.show();
            var _a = wx.getSystemInfoSync(), screenWidth = _a.screenWidth, screenHeight_1 = _a.screenHeight;
            bannerAd.onResize(function (res) {
                // banner广告放在底部
                bannerAd.style.top = screenHeight_1 - bannerAd.style.realHeight;
            });
            bannerAd.style.width = screenWidth;
        }
        return bannerAd;
    };
    // private static code = '';
    WxKit.iv = '';
    WxKit.enctypecode = '';
    /**
     * 流量主视频广告调用方法
     *
     */
    WxKit.video_ads = {};
    WxKit.current_video_ad_id = '';
    return WxKit;
}());
__reflect(WxKit.prototype, "WxKit");
