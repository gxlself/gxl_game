var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var Utils = (function () {
    function Utils() {
    }
    Utils.getNowDate = function () {
        return this.dateFtt(new Date(), "yyyy-MM-dd hh:mm:ss");
    };
    Utils.getWeekTime = function () {
        var now = new Date();
        var y = now.getFullYear();
        var m = now.getMonth();
        var d = now.getDate();
        var day = now.getDay();
        var weekStart = new Date(y, m, d - (day ? (day - 1) : 6));
        var weekEnd = new Date(y, m, d + (day ? (8 - day) : 1));
        Utils.dateRange = [this.dateFtt(weekStart), this.dateFtt(weekEnd)];
        return Utils.dateRange;
    };
    Utils.isInTimeRange = function (timeStr) {
        if (timeStr) {
            var recordTime = new Date(timeStr.replace(/-/g, '/')).getTime();
            return recordTime > this.dateRange[0] && recordTime < this.dateRange[1];
        }
        else {
            return false;
        }
    };
    Utils.transObj = function (kvData) {
        var res = {};
        kvData.map(function (data) {
            res[data.key] = data.value;
        });
        return res;
    };
    // 时间格式处理
    Utils.dateFtt = function (date, fmt) {
        var o = {
            "M+": date.getMonth() + 1,
            "d+": date.getDate(),
            "h+": date.getHours(),
            "m+": date.getMinutes(),
            "s+": date.getSeconds(),
            "q+": Math.floor((date.getMonth() + 3) / 3),
            "S": date.getMilliseconds() //毫秒   
        };
        fmt || (fmt = "yyyy-MM-ddThh:mm:ss+00:00");
        if (/(y+)/.test(fmt))
            fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o)
            if (new RegExp("(" + k + ")").test(fmt))
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    };
    Utils.dateRange = [];
    return Utils;
}());
__reflect(Utils.prototype, "Utils");
