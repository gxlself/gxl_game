var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var Mp3 = (function () {
    function Mp3() {
    }
    // 初始化音轨
    Mp3.loadEventSound = function () {
        this.eventSoundList = SoundRes.eventSoundList.map(function (soundData) {
            if (!soundData['cnt']) {
                soundData['context'] = wx.createInnerAudioContext();
                soundData['context'].src = soundData.path;
                return soundData;
            }
            else {
                var obj = { name: soundData.name, soundArr: [] };
                for (var i = 0, l = soundData['cnt']; i < l; i++) {
                    var res = {};
                    res['context'] = wx.createInnerAudioContext();
                    res['context'].src = soundData.path;
                    obj['soundArr'].push(res);
                }
                return obj;
            }
        });
        console.log(this.eventSoundList);
    };
    // 切换背景音乐
    Mp3.switchBgm = function (bgm_name) {
        var _this = this;
        this.bgm && this.bgm.pause();
        this.eventSoundList.map(function (soundData) {
            if (soundData.name == bgm_name) {
                if (soundData['context']) {
                    _this.bgm = soundData['context'];
                }
                else {
                    _this.bgm = soundData['soundArr'][0]['context'];
                }
            }
        });
        this.bgm && (this.bgm.loop = true);
        this.bgm && !this.mute && this.bgm.play();
    };
    //播放背景音乐
    Mp3.playBGM = function () {
        this.bgm && !this.mute && this.bgm.play();
    };
    // 停止背景音乐
    Mp3.stopBGM = function () {
        this.bgm && this.bgm.pause();
    };
    // 播放事件音效
    Mp3.playEvent = function (event_name) {
        var _this = this;
        this.eventSoundList.map(function (soundData) {
            console.log(soundData.name);
            if (soundData.name == event_name) {
                // console.log(soundData['context'])
                if (soundData['context']) {
                    soundData['context'].src = 'resource/assets/subpackage/mp3/bgm.mp3';
                    !_this.mute && soundData['context'].play();
                }
                else {
                    var target = soundData['soundArr'][0];
                    soundData['soundArr'].push(soundData['soundArr'].shift());
                    !_this.mute && target['context'].play();
                }
            }
        });
    };
    Mp3.bgm = null;
    Mp3.eventSoundList = null;
    // 静音标识
    Mp3.mute = false;
    return Mp3;
}());
__reflect(Mp3.prototype, "Mp3");
