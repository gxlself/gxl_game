var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var partRes = (function () {
    function partRes() {
    }
    partRes.initList = function () {
        this.list.map(function (res) {
            res.texture = RES.getRes(res.texture_path);
            res.json = RES.getRes(res.json_path);
        });
    };
    partRes.getRes = function (name) {
        var result = { texture: null, json: null };
        this.list.map(function (res) {
            if (res.name == name) {
                result = res;
            }
        });
        return result;
    };
    // 定义事件音乐路径，Mp3.playEvent调用name即可播放对应path的音乐
    partRes.list = [
        { name: 'boom', texture_path: 'boom_png', texture: null, json_path: 'boom_json', json: null },
        { name: 'hit', texture_path: 'hit_png', texture: null, json_path: 'hit_json', json: null },
        { name: 'flame', texture_path: 'flame_png', texture: null, json_path: 'flame_json', json: null },
    ];
    return partRes;
}());
__reflect(partRes.prototype, "partRes");
