// 音频资源配置JSON
var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var SoundRes = (function () {
    function SoundRes() {
    }
    // 定义事件音乐路径，Mp3.playEvent调用name即可播放对应path的音乐
    SoundRes.eventSoundList = [
        { name: 'bgm_default', path: 'resource/assets/subpackage/mp3/bgm.mp3' },
        { name: 'bgm_game', path: 'resource/assets/subpackage/mp3/bgm.mp3' },
        // 高频调用的音效，如子弹射击/击中音效等，进行多音轨队列播放，用 cnt 控制音轨队列容量
        { name: 'hit', path: 'resource/assets/subpackage/mp3/bgm.mp3', cnt: 10 }
    ];
    return SoundRes;
}());
__reflect(SoundRes.prototype, "SoundRes");
