var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var UserData = (function () {
    function UserData() {
    }
    UserData.getOpenId = function () {
        return this.openId;
    };
    UserData.getId = function () {
        return this.id;
    };
    UserData.getUserData = function () {
        return JSON.parse(JSON.stringify(this.userInfo));
    };
    UserData.setUserData = function (userData) {
        this.avatar_url = userData.avatar_url;
        this.openId = userData.open_id;
        this.nickname = userData.nickname;
        this.gender = userData.gender;
        this.id = userData.id;
        this.userInfo = userData;
    };
    return UserData;
}());
__reflect(UserData.prototype, "UserData");
