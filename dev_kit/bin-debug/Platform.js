/**
 * 平台数据接口。
 * 由于每款游戏通常需要发布到多个平台上，所以提取出一个统一的接口用于开发者获取平台数据信息
 * 推荐开发者通过这种方式封装平台逻辑，以保证整体结构的稳定
 * 由于不同平台的接口形式各有不同，白鹭推荐开发者将所有接口封装为基于 Promise 的异步形式
 */
var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var WxPlatform = (function () {
    function WxPlatform() {
        this.name = 'wxgame';
        this.header = {};
    }
    // 开放域获取好友排行
    WxPlatform.prototype.getFriendCloudStorage = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        wx.getFriendCloudStorage({
                            keyList: ["socre", "date"],
                            success: function (res) {
                                console.log(res);
                                resolve(res);
                            },
                            fail: function (err) {
                                reject(err);
                            }
                        });
                    })];
            });
        });
    };
    // 开放域获取群组排行
    WxPlatform.prototype.getGroupCloudStorage = function (shareTicket) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        wx.getGroupCloudStorage({
                            keyList: ["score", "date"],
                            success: function (res) {
                                // console.log(res.data);
                                var data = new Array();
                                if (res.data && res.data.length != 0) {
                                    res.data.forEach(function (item, index) {
                                        var kvData = Utils.transObj(item.KVDataList);
                                        if (Utils.isInTimeRange(kvData['date'])) {
                                            data.push({
                                                score: parseInt(kvData['score']),
                                                date: kvData['date'] || '',
                                                openId: item.openid,
                                                user: {
                                                    nickname: item.nickname,
                                                    avatar_url: item.avatarUrl
                                                }
                                            });
                                        }
                                    });
                                    data.sort(function (a, b) {
                                        var valueA = a['score'];
                                        var valueB = b['score'];
                                        return valueB - valueA;
                                    });
                                }
                                resolve(data);
                            },
                            fail: function (err) {
                                reject(err);
                            },
                            complete: function (res) {
                            },
                            shareTicket: shareTicket
                        });
                    })];
            });
        });
    };
    WxPlatform.prototype.setKVData = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        var dataList = [];
                        for (var key in data) {
                            dataList.push({ key: key, value: data[key] });
                        }
                        wx.setUserCloudStorage({
                            KVDataList: dataList,
                            success: function (res) {
                                resolve(res);
                            },
                            fail: function (err) {
                                reject(err);
                            }
                        });
                    })];
            });
        });
    };
    WxPlatform.prototype.login = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        wx.showLoading({
                            title: '游戏加载中...',
                            mask: true
                        });
                        wx.login({
                            success: function (res) {
                                resolve(res);
                            }, fail: function (err) {
                                reject(err);
                            }
                        });
                    })];
            });
        });
    };
    WxPlatform.prototype.auth = function (jsCode, iv, encrytedData) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        var data = {
                            js_code: jsCode,
                            iv: iv,
                            encrypted_data: encrytedData
                        };
                        var that = _this;
                        wx.request({
                            url: Api.loginPath,
                            method: 'POST',
                            data: data,
                            success: function (res) {
                                that.header = {
                                    Authorization: res.data.token
                                };
                                wx.hideLoading();
                                resolve(res);
                            },
                            fail: function (err) {
                                reject(err);
                            }
                        });
                    })];
            });
        });
    };
    WxPlatform.prototype.getUserInfo = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        wx.getUserInfo({
                            success: function (res) {
                                var userInfo = res.userInfo;
                                var nickName = userInfo.nickName;
                                var avatarUrl = userInfo.avatarUrl;
                                var gender = userInfo.gender; //性别 0：未知、1：男、2：女
                                var province = userInfo.province;
                                var city = userInfo.city;
                                var country = userInfo.country;
                                userInfo.encryptedData = res.encryptedData;
                                userInfo.iv = res.iv;
                                resolve(userInfo);
                            }, fail: function (err) {
                                reject(err);
                            }
                        });
                    })];
            });
        });
    };
    WxPlatform.prototype.showAuthModal = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                wx.hideLoading();
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        wx.showModal({
                            title: '提示',
                            content: '请您进行登陆授权',
                            showCancel: false,
                            cancelText: '',
                            confirmText: '去授权',
                            success: function (res) {
                                wx.openSetting({
                                    success: function (result) {
                                        resolve(result);
                                    },
                                    fail: function (err) {
                                        reject(err);
                                    }
                                });
                            },
                        });
                    })];
            });
        });
    };
    return WxPlatform;
}());
__reflect(WxPlatform.prototype, "WxPlatform", ["Platform"]);
if (!window.platform) {
    window.platform = new WxPlatform();
}
