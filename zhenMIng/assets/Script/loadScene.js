cc.Class({
    extends: cc.Component,

    properties: {
        
    },
    onLoad () {
        let _this = this
        cc.log("开始加载资源")
        cc.myAssets = {}
        cc.loader.loadResDir('Index', function (err, assets) {
            if (err) {
                cc.error(err);
                return;
            }
            cc.myAssets['Index'] = assets;
            cc.director.loadScene("Index")
            cc.log("加载资源结束")
        });
    },
    start () {

    }
});
